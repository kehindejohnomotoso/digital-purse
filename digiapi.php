<?php
include_once "./digitalpurse.php";
$purse = new DigitalPurse();
$a = "";
if (isset($_POST["a"])) {
    $a = $_POST["a"];
}
if (isset($_GET["a"])) {
    $a = $_GET["a"];
}
$b = "";
if (isset($_POST["b"])) {
    $b = $_POST["b"];
}
if (isset($_GET["b"])) {
    $b = $_GET["b"];
}
$c = "";
if (isset($_POST["c"])) {
    $c = $_POST["c"];
}
if (isset($_GET["c"])) {
    $c = $_GET["c"];
}


$d = "";
if (isset($_POST["d"])) {
    $d = $_POST["d"];
}
if (isset($_GET["d"])) {
    $d = $_GET["d"];
}

$e = "";
if (isset($_POST["e"])) {
    $e = $_POST["e"];
}
if (isset($_GET["e"])) {
    $e = $_GET["e"];
}

$f = "";
if (isset($_POST["f"])) {
    $f = $_POST["f"];
}
if (isset($_GET["f"])) {
    $f = $_GET["f"];
}

$g = "";
if (isset($_POST["g"])) {
    $g = $_POST["g"];
}
if (isset($_GET["g"])) {
    $g = $_GET["g"];
}

$h = "";
if (isset($_POST["h"])) {
    $h = $_POST["h"];
}
if (isset($_GET["h"])) {
    $h = $_GET["h"];
}

$i = "";
if (isset($_POST["i"])) {
    $i = $_POST["i"];
}
if (isset($_GET["i"])) {
    $i = $_GET["i"];
}


$j = "";
if (isset($_POST["j"])) {
    $j = $_POST["j"];
}
if (isset($_GET["j"])) {
    $j = $_GET["j"];
}


$k = "";
if (isset($_POST["k"])) {
    $k = $_POST["k"];
}
if (isset($_GET["k"])) {
    $k = $_GET["k"];
}


$l = "";
if (isset($_POST["l"])) {
    $l = $_POST["l"];
}
if (isset($_GET["l"])) {
    $l = $_GET["l"];
}

$m = "";
if (isset($_POST["m"])) {
    $m = $_POST["m"];
}
if (isset($_GET["m"])) {
    $m = $_GET["m"];
}

/////////
if (isset($_GET["draw"])) {
    $c = $_GET["draw"];
}
if (isset($_GET["start"])) {
    $d = $_GET["start"];
}
if (isset($_GET["length"])) {
    $e = $_GET["length"];
}
$runData = new stdClass();
$runData->a = $a;
$runData->b = $b;
$runData->c = $c;
$runData->d = $d;
$runData->e = $e;
$runData->f = $f;
$runData->g = $g;
$runData->h = $h;
$runData->i = $i;
$runData->j = $j;
$runData->k = $k;
$runData->l = $l;
$runData->m = $m;

if (isset($_SERVER['HTTP_ORIGIN'])) {
    // Decide if the origin in $_SERVER['HTTP_ORIGIN'] is one
    // you want to allow, and if so:
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
//    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

header('Content-type: application/json');
//$response = json_encode($runData);
$response = json_encode($purse->runApi($runData));
echo $response;