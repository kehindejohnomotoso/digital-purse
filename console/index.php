<?php
session_start();
$overviewData = new stdClass();
if (!isset($_SESSION['uiLJKukykhHKVulvjhLKOUHcgVGHGfvhVGHchlv'])) {
    header("Location:login/");
} else {
    $username = $_SESSION['uiLJKukykhHKVulvjhLKOUHcgVGHGfvhVGHchlv'];
    include_once "../digitalpurse.php";
    $purse = new DigitalPurse();
    $runData = new stdClass();
    $overviewData = $purse->getOverviewData($username);
}
?>
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <meta name="author" content="Kehinde Omotoso">
    <meta name="author-email" content="kehindejohnomotoso@gmail.com">
    <title>eWallet System</title>
    <link rel="apple-touch-icon" href="app-assets/images/ico/apple-icon-120.html">
    <link rel="shortcut icon" type="image/x-icon"
          href="https://pixinvent.com/demo/vuexy-html-bootstrap-admin-template/app-assets/images/ico/favicon.ico">
    <link
            href="fonts.googleapis.com/css219a5.css?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600"
            rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/charts/apexcharts.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/extensions/toastr.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap-extended.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/colors.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/components.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/themes/dark-layout.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/themes/bordered-layout.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/themes/semi-dark-layout.min.css">


    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/responsive.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css">
    <link
            rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/rowGroup.bootstrap4.min.css">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/vertical-menu.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/pages/dashboard-ecommerce.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/charts/chart-apex.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/extensions/ext-component-toastr.min.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link
            rel="stylesheet" type="text/css" href="assets/css/style.css"> <!-- END: Custom CSS-->

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->
<body class="vertical-layout vertical-menu-modern  navbar-floating footer-static  " data-open="click"
      data-menu="vertical-menu-modern" data-col="">

<!-- END: Header-->


<!-- BEGIN: Main Menu-->
<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto">
                <a class="navbar-brand" href="">
                            <span class="brand-logo">
                                <svg viewbox="0 0 139 95" version="1.1" xmlns="http://www.w3.org/2000/svg"
                                     height="24">
                                    <defs>
                                        <lineargradient id="linearGradient-1" x1="100%" y1="10.5120544%" x2="50%"
                                                        y2="89.4879456%">
                                            <stop stop-color="#000000" offset="0%"></stop>
                                            <stop stop-color="#FFFFFF" offset="100%"></stop>
                                        </lineargradient>
                                        <lineargradient id="linearGradient-2" x1="64.0437835%" y1="46.3276743%"
                                                        x2="37.373316%" y2="100%">
                                            <stop stop-color="#EEEEEE" stop-opacity="0" offset="0%"></stop>
                                            <stop stop-color="#FFFFFF" offset="100%"></stop>
                                        </lineargradient>
                                    </defs>
                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <g id="Artboard" transform="translate(-400.000000, -178.000000)">
                                            <g id="Group" transform="translate(400.000000, 178.000000)">
                                                <path class="text-primary" id="Path"
                                                      d="M-5.68434189e-14,2.84217094e-14 L39.1816085,2.84217094e-14 L69.3453773,32.2519224 L101.428699,2.84217094e-14 L138.784583,2.84217094e-14 L138.784199,29.8015838 C137.958931,37.3510206 135.784352,42.5567762 132.260463,45.4188507 C128.736573,48.2809251 112.33867,64.5239941 83.0667527,94.1480575 L56.2750821,94.1480575 L6.71554594,44.4188507 C2.46876683,39.9813776 0.345377275,35.1089553 0.345377275,29.8015838 C0.345377275,24.4942122 0.230251516,14.560351 -5.68434189e-14,2.84217094e-14 Z"
                                                      style="fill:currentColor"></path>
                                                <path id="Path1"
                                                      d="M69.3453773,32.2519224 L101.428699,1.42108547e-14 L138.784583,1.42108547e-14 L138.784199,29.8015838 C137.958931,37.3510206 135.784352,42.5567762 132.260463,45.4188507 C128.736573,48.2809251 112.33867,64.5239941 83.0667527,94.1480575 L56.2750821,94.1480575 L32.8435758,70.5039241 L69.3453773,32.2519224 Z"
                                                      fill="url(#linearGradient-1)" opacity="0.2"></path>
                                                <polygon id="Path-2" fill="#000000" opacity="0.049999997"
                                                         points="69.3922914 32.4202615 32.8435758 70.5039241 54.0490008 16.1851325"></polygon>
                                                <polygon id="Path-21" fill="#000000" opacity="0.099999994"
                                                         points="69.3922914 32.4202615 32.8435758 70.5039241 58.3683556 20.7402338"></polygon>
                                                <polygon id="Path-3" fill="url(#linearGradient-2)" opacity="0.099999994"
                                                         points="101.428699 0 83.0667527 94.1480575 130.378721 47.0740288"></polygon>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                            </span>
                    <h2 class="brand-text">eWallet</h2>
                </a>
            </li>
        </ul>
    </div>
    <div class="shadow-bottom"></div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class=" navigation-header">
                <span data-i18n="Apps &amp; Pages">Application Menu</span>
                <i data-feather="more-horizontal"></i>
            </li>
            <li class="nav-item sidebar-group-active">
                <a class="d-flex align-items-center" href="">
                    <i data-feather='activity'></i>
                    <span class="menu-title text-truncate" data-i18n="Overview">Overview</span>
                </a>
            </li>
            <li class=" nav-item">
                <a class="d-flex align-items-center" href="users/">
                    <i data-feather='users'></i>
                    <span class="menu-title text-truncate" data-i18n="Overview">eWallet Users</span>
                </a>
            </li>
            <li class=" nav-item">
                <a class="d-flex align-items-center" href="transactions/">
                    <i data-feather='percent'></i>
                    <span class="menu-title text-truncate" data-i18n="Overview">eWallet Transactions</span>
                </a>
            </li>
            <li class=" nav-item">
                <a class="d-flex align-items-center" href="institution/">
                    <i data-feather='home'></i>
                    <span class="menu-title text-truncate" data-i18n="Overview">Institutions</span>
                </a>
            </li>
            <li class=" nav-item">
                <a class="d-flex align-items-center" href="stakeholders/">
                    <i data-feather='user-check'></i>
                    <span class="menu-title text-truncate" data-i18n="Overview">eWallet Stakeholders</span>
                </a>
            </li>
            <li class=" nav-item">
                <a class="d-flex align-items-center" href="courses/">
                    <i data-feather='book-open'></i>
                    <span class="menu-title text-truncate" data-i18n="Overview">Courses / Products</span>
                </a>
            </li>
            <!--            <li class=" nav-item">-->
            <!--                <a class="d-flex align-items-center" href="withdrawal/">-->
            <!--                    <i data-feather='dollar-sign'></i>-->
            <!--                    <span class="menu-title text-truncate" data-i18n="Overview">Withdrawal Requests</span>-->
            <!--                </a>-->
            <!--            </li>-->
        </ul>
    </div>
</div>
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content ">
    <!--    <div class="content-overlay"></div>-->
    <!--    <div class="header-navbar-shadow"></div>-->

    <div class="content-overlay"></div>
    <div class="header-navbar-shadow">
        <div class="nav-flex-right">
            <div>
                <!--                <h6>Welcome, --><?php //echo $_SESSION['fn'] ?><!--</h6>-->
            </div>
            <div>
                <a class="d-flex align-items-center" href="logout/">
                    <i data-feather='log-out'></i>
                    <span class="menu-title text-truncate" data-i18n="Overview"> &nbsp;Log Out</span>
                </a>
            </div>
        </div>
    </div>

    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">
                            <i data-feather='activity'></i>
                            Overview</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Dashboard Ecommerce Starts -->
            <section id="dashboard-ecommerce">
                <div
                        class="row match-height">
                    <!-- Medal Card -->
                    <div class="col-xl-6 col-md-6 col-12">
                        <div class="card card-congratulations">
                            <div class="card-body text-center">
                                <img src="app-assets/images/elements/decore-left.png" class="congratulations-img-left"
                                     alt="card-img-left">
                                <img src="app-assets/images/elements/decore-right.png" class="congratulations-img-right"
                                     alt="card-img-right">
                                <div class="text-center index_500">
                                    <h1 class="mb-1 text-white">Welcome <?php echo $overviewData->user->name; ?>,</h1>
                                    <p class="card-text m-auto w-75">
                                        You have
                                        <strong id="withdrawable"></strong>
                                        as your compensation from wallet subscriptions so far.
                                        <br/><br/>
                                        <!--                                        <button type="button" class="btn btn-gradient-danger">Request Withdrawal-->
                                        <!--                                            <i data-feather='credit-card'></i>-->
                                        <!--                                        </button>-->
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/ Medal Card -->

                    <!-- Statistics Card -->
                    <div class="col-xl-6 col-md-6 col-12">
                        <div class="card card-statistics">
                            <div class="card-header">
                                <h4 class="card-title">Statistics</h4>
                            </div>
                            <div class="card-body statistics-body">
                                <div class="row">
                                    <?php echo $overviewData->user->userType === "administrator" || $overviewData->user->userType === "institution" ? '<div class="col-xl-4 col-sm-6 col-12 mb-2 mb-xl-0">
                                        <div class="media">
                                            <div class="avatar bg-light-primary mr-2">
                                                <div class="avatar-content">
                                                    <i data-feather="briefcase" class="avatar-icon"></i>
                                                </div>
                                            </div>
                                            <div class="media-body my-auto">
                                                <h4 class="font-weight-bolder mb-0">' . $overviewData->stats->institutions . '</h4>
                                                <p class="card-text font-small-3 mb-0">Institutions</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-4 col-sm-6 col-12 mb-2 mb-xl-0">
                                        <div class="media">
                                            <div class="avatar bg-light-info mr-2">
                                                <div class="avatar-content">
                                                    <i data-feather="user" class="avatar-icon"></i>
                                                </div>
                                            </div>
                                            <div class="media-body my-auto">
                                                <h4 class="font-weight-bolder mb-0">' . $overviewData->stats->stakeholders . '</h4>
                                                <p class="card-text font-small-3 mb-0">Stakeholders</p>
                                            </div>
                                        </div>
                                    </div>' : ''; ?>
                                    <div class="col-xl-4 col-sm-6 col-12">
                                        <div class="media">
                                            <div class="avatar bg-light-success mr-2">
                                                <div class="avatar-content">
                                                    <i data-feather="box" class="avatar-icon"></i>
                                                </div>
                                            </div>
                                            <div class="media-body my-auto">
                                                <h4 class="font-weight-bolder mb-0"><?php echo $overviewData->stats->products ?></h4>
                                                <p class="card-text font-small-3 mb-0">Products</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/ Statistics Card -->
                </div>

                <div class="row">
                    <div class="col-lg-4 col-sm-6 col-12">
                        <div class="card p-bottom-10">
                            <div class="card-header align-items-start pb-25">
                                <div>
                                    <h2 class="font-weight-bolder">&#8358;<span
                                                id="unspent-wallet-funds">0</span>
                                    </h2>
                                    <p class="card-text">Funds in Circulation [Unspent Wallet Funds]</p>
                                </div>
                                <div class="avatar bg-light-primary p-50">
                                    <div class="avatar-content">
                                        <i data-feather='repeat' class="avatar-icon"></i>
                                    </div>
                                </div>
                            </div>
                            <!--                            Extra Chart Goes Here-->
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 col-12">
                        <div class="card p-bottom-10">
                            <div class="card-header align-items-start pb-25">
                                <div>
                                    <h2 class="font-weight-bolder">
                                        &#8358;<span id="spent-wallet-funds">0</span></h2>
                                    <p class="card-text">Total Spent Wallet Funds - Students</p>
                                </div>
                                <div class="avatar bg-light-success p-50">
                                    <div class="avatar-content">
                                        <i data-feather='credit-card'></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 col-12">
                        <div class="card p-bottom-10">
                            <div class="card-header align-items-start pb-lg-25">
                                <div>
                                    <h2 class="font-weight-bolder"><?= $purse->getActiveSubscriptionCount() ?></h2>
                                    <p class="card-text">Active Subscriptions</p>
                                </div>
                                <div class="avatar bg-light-warning p-50">
                                    <div class="avatar-content">
                                        <i data-feather='airplay'></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="overview-chart" class="row match-height">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header d-flex flex-sm-row flex-column justify-content-md-between align-items-start justify-content-start">
                                <div>
                                    <h4 class="card-title">Monthly Subscriptions</h4>
                                    <span class="card-subtitle text-muted">Active Subscriptions per Month</span>
                                </div>
                            </div>
                            <div class="card-body">
                                <div id="timeline-chart"></div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
<!-- END: Content-->


<!-- BEGIN: Vendor JS-->
<script src="app-assets/vendors/js/vendors.min.js"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
<script src="app-assets/vendors/js/charts/apexcharts.min.js"></script>
<script src="app-assets/vendors/js/extensions/toastr.min.js"></script>
<!-- <script src="app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js"></script> -->
<!-- END: Page Vendor JS-->


<!-- BEGIN: Theme JS-->
<script src="app-assets/js/core/app-menu.min.js"></script>
<script src="app-assets/js/core/app.min.js"></script>
<!-- END: Theme JS-->


<!-- BEGIN: Page JS-->
<script src="app-assets/js/scripts/charts/chart-apex.min.js"></script>

<script src="assets/js/app.js"></script>

<script>
    $(window).on('load', function () {
        if (feather) {
            feather.replace({width: 14, height: 14});
        }
    })
    $("#withdrawable").html("&#8358;" + formatMoney(<?php echo $overviewData->balance ?>));

    $("#spent-wallet-funds").html(formatMoney(<?php echo $overviewData->transactionBalance->spent; ?>));
    $("#unspent-wallet-funds").html(formatMoney(<?php echo $overviewData->transactionBalance->unspent; ?>));

    generateDashboardChart(<?= json_encode($overviewData->subscriptionStats->currentYear) ?>, <?= json_encode($overviewData->subscriptionStats->previousYear) ?>);
</script>
</body>
</body>
</html>
