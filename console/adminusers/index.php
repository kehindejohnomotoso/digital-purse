<?php
session_start();
include_once "../assets/includeJS.php";


$selectDropDown = array();
if (!isset($_SESSION['uiLJKukykhHKVulvjhLKOUHcgVGHGfvhVGHchlv'])) {
    header("Location:../login/");
} else {
    $username = $_SESSION['uiLJKukykhHKVulvjhLKOUHcgVGHGfvhVGHchlv'];
    include_once "../../digitalpurse.php";
    $purse = new DigitalPurse();
    $runData = new stdClass();
    $selectDropDown = $purse->getInstitutionsAsSelect()->institutions;
}

?>
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <meta name="author" content="Kehinde Omotoso">
    <meta name="author-email" content="kehindejohnomotoso@gmail.com">
    <title>Admin Users - eWallet System</title>
    <link rel="apple-touch-icon" href="../app-assets/images/ico/apple-icon-120.html">
    <link rel="shortcut icon" type="image/x-icon"
          href="https://pixinvent.com/demo/vuexy-html-bootstrap-admin-template/../app-assets/images/ico/favicon.ico">
    <link
            href="../fonts.googleapis.com/css219a5.css?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600"
            rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/charts/apexcharts.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/extensions/toastr.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="../app-assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/bootstrap-extended.min.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/colors.min.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/components.min.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/themes/dark-layout.min.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/themes/bordered-layout.min.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/themes/semi-dark-layout.min.css">


    <link rel="stylesheet" type="text/css"
          href="../app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css"
          href="../app-assets/vendors/css/tables/datatable/responsive.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css">
    <link
            rel="stylesheet" type="text/css"
            href="../app-assets/vendors/css/tables/datatable/rowGroup.bootstrap4.min.css">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="../app-assets/css/core/menu/menu-types/vertical-menu.min.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/pages/dashboard-ecommerce.min.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/plugins/charts/chart-apex.min.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/plugins/extensions/ext-component-toastr.min.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link
            rel="stylesheet" type="text/css" href="../assets/css/style.css"> <!-- END: Custom CSS-->

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->
<body
        class="vertical-layout vertical-menu-modern  navbar-floating footer-static  " data-open="click"
        data-menu="vertical-menu-modern" data-col="">

<!--<div class="loadingOverlay">-->
<!--    <div class="spinner-grow" role="status">-->
<!--        <span class="sr-only">Loading, Please wait...</span>-->
<!--    </div>-->
<!--    <div class="fail-retry">-->
<!--        <p>Some network error has occurred.</p>-->
<!--        <a href="">Please try Again</a>-->
<!--    </div>-->
<!--</div>-->

<!-- BEGIN: Header-->

<!-- END: Header-->


<!-- BEGIN: Main Menu-->
<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto">
                <a class="navbar-brand" href="">
                            <span class="brand-logo">
                                <svg viewbox="0 0 139 95" version="1.1" xmlns="http://www.w3.org/2000/svg"
                                     height="24">
                                    <defs>
                                        <lineargradient id="linearGradient-1" x1="100%" y1="10.5120544%" x2="50%"
                                                        y2="89.4879456%">
                                            <stop stop-color="#000000" offset="0%"></stop>
                                            <stop stop-color="#FFFFFF" offset="100%"></stop>
                                        </lineargradient>
                                        <lineargradient id="linearGradient-2" x1="64.0437835%" y1="46.3276743%"
                                                        x2="37.373316%" y2="100%">
                                            <stop stop-color="#EEEEEE" stop-opacity="0" offset="0%"></stop>
                                            <stop stop-color="#FFFFFF" offset="100%"></stop>
                                        </lineargradient>
                                    </defs>
                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <g id="Artboard" transform="translate(-400.000000, -178.000000)">
                                            <g id="Group" transform="translate(400.000000, 178.000000)">
                                                <path class="text-primary" id="Path"
                                                      d="M-5.68434189e-14,2.84217094e-14 L39.1816085,2.84217094e-14 L69.3453773,32.2519224 L101.428699,2.84217094e-14 L138.784583,2.84217094e-14 L138.784199,29.8015838 C137.958931,37.3510206 135.784352,42.5567762 132.260463,45.4188507 C128.736573,48.2809251 112.33867,64.5239941 83.0667527,94.1480575 L56.2750821,94.1480575 L6.71554594,44.4188507 C2.46876683,39.9813776 0.345377275,35.1089553 0.345377275,29.8015838 C0.345377275,24.4942122 0.230251516,14.560351 -5.68434189e-14,2.84217094e-14 Z"
                                                      style="fill:currentColor"></path>
                                                <path id="Path1"
                                                      d="M69.3453773,32.2519224 L101.428699,1.42108547e-14 L138.784583,1.42108547e-14 L138.784199,29.8015838 C137.958931,37.3510206 135.784352,42.5567762 132.260463,45.4188507 C128.736573,48.2809251 112.33867,64.5239941 83.0667527,94.1480575 L56.2750821,94.1480575 L32.8435758,70.5039241 L69.3453773,32.2519224 Z"
                                                      fill="url(#linearGradient-1)" opacity="0.2"></path>
                                                <polygon id="Path-2" fill="#000000" opacity="0.049999997"
                                                         points="69.3922914 32.4202615 32.8435758 70.5039241 54.0490008 16.1851325"></polygon>
                                                <polygon id="Path-21" fill="#000000" opacity="0.099999994"
                                                         points="69.3922914 32.4202615 32.8435758 70.5039241 58.3683556 20.7402338"></polygon>
                                                <polygon id="Path-3" fill="url(#linearGradient-2)" opacity="0.099999994"
                                                         points="101.428699 0 83.0667527 94.1480575 130.378721 47.0740288"></polygon>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                            </span>
                    <h2 class="brand-text">eWallet</h2>
                </a>
            </li>
        </ul>
    </div>
    <div class="shadow-bottom"></div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class=" navigation-header">
                <span data-i18n="Apps &amp; Pages">Application Menu</span>
                <i data-feather="more-horizontal"></i>
            </li>
            <li class="nav-item">
                <a class="d-flex align-items-center" href="../">
                    <i data-feather='activity'></i>
                    <span class="menu-title text-truncate" data-i18n="Overview">Overview</span>
                </a>
            </li>
            <li class=" nav-item sidebar-group-active">
                <a class="d-flex align-items-center" href="../users/">
                    <i data-feather='users'></i>
                    <span class="menu-title text-truncate" data-i18n="Overview">eWallet Users</span>
                </a>
            </li>
            <li class=" nav-item">
                <a class="d-flex align-items-center" href="../transactions/">
                    <i data-feather='percent'></i>
                    <span class="menu-title text-truncate" data-i18n="Overview">eWallet Transactions</span>
                </a>
            </li>
            <li class=" nav-item">
                <a class="d-flex align-items-center" href="../institution/">
                    <i data-feather='home'></i>
                    <span class="menu-title text-truncate" data-i18n="Overview">Institutions</span>
                </a>
            </li>
            <li class=" nav-item">
                <a class="d-flex align-items-center" href="../stakeholders/">
                    <i data-feather='user-check'></i>
                    <span class="menu-title text-truncate" data-i18n="Overview">eWallet Stakeholders</span>
                </a>
            </li>
            <li class=" nav-item">
                <a class="d-flex align-items-center" href="../courses/">
                    <i data-feather='book-open'></i>
                    <span class="menu-title text-truncate" data-i18n="Overview">Courses / Products</span>
                </a>
            </li>
<!--            <li class=" nav-item">-->
<!--                <a class="d-flex align-items-center" href="../withdrawal/">-->
<!--                    <i data-feather='dollar-sign'></i>-->
<!--                    <span class="menu-title text-truncate" data-i18n="Overview">Withdrawal Requests</span>-->
<!--                </a>-->
<!--            </li>-->
        </ul>
    </div>
</div>
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-12 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12 flex flex-row justify-content-between width-block">
                        <h2 class="content-header-title mb-0">
                            <i data-feather='users'></i>
                            eWallet Users</h2>
<!--                        <button type="button" class="create-new btn btn-primary" data-toggle="modal"-->
<!--                                data-target="#create-user-modal">-->
<!--                            <i data-feather='plus'></i>-->
<!--                            Create Wallet User-->
<!--                        </button>-->
                    </div>
                </div>
            </div>
        </div>
        <div
                class="content-body">
            <!-- Dashboard Ecommerce Starts -->
            <section
                    id="dashboard-ecommerce">

                <!-- Basic table -->
                <section id="basic-datatable">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body card-datatable table-responsive pt-0">
                                    <table class="user-list-table table-hover-animation table-hover table"
                                           id="users-table">

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="modal modal-slide-in fade" id="create-user-modal">
                        <div class="modal-dialog sidebar-sm">
                            <form class="add-new-record modal-content pt-0" id="create-user-form">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
                                <div class="modal-header mb-1">
                                    <h5 class="modal-title" id="exampleModalLabel">Create Wallet User</h5>
                                </div>
                                <div class="modal-body flex-grow-1">
                                    <div class="form-group">
                                        <label class="form-label" for="basic-icon-default-fullname">Full Name *</label>
                                        <input type="text" class="form-control dt-full-name" id="user-user-fullname"
                                               placeholder="" aria-label=""/>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label" for="basic-icon-default-email">Email Address *</label>
                                        <input type="email" id="user-user-email" class="form-control dt-email"
                                               placeholder="" aria-label=""/>
                                        <small class="form-text text-muted">
                                            You can use letters, numbers & periods
                                        </small>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label" for="basic-icon-default-post">Username *</label>
                                        <input type="text" id="user-user-username" class="form-control dt-post"
                                               placeholder="" aria-label=""/>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label" for="basic-icon-default-fullname">User Type /
                                            Role</label>
                                        <select class="form-control dt-access-type" id="user-access-type">
                                            <option value="0">Student Access</option>
                                            <option value="1">Institution Access</option>
                                            <option value="2">Stakeholder Access</option>
                                            <option value="3">System Administrator Access</option>
                                        </select>
                                    </div>
                                    <button type="submit" class="btn btn-primary data-submit mr-1">Create User</button>
                                    <button type="reset" class="btn btn-outline-secondary" data-dismiss="modal">Cancel
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
                <!--/ Basic table -->

        </div>
    </div>
</div>
<!-- END: Content-->


<!-- BEGIN: Vendor JS-->
<script src="../app-assets/vendors/js/vendors.min.js"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
<script src="../app-assets/vendors/js/charts/apexcharts.min.js"></script>
<script src="../app-assets/vendors/js/extensions/toastr.min.js"></script>
<!-- <script src="../app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js"></script> -->
<!-- END: Page Vendor JS-->


<!-- BEGIN: Theme JS-->
<script src="../app-assets/js/core/app-menu.min.js"></script>
<script src="../app-assets/js/core/app.min.js"></script>
<!-- END: Theme JS-->


<!-- BEGIN: Page JS-->
<script src="../app-assets/js/scripts/charts/chart-apex.min.js"></script>


<script src="../app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js"></script>
<script src="../app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js"></script>
<script src="../app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js"></script>
<script src="../app-assets/vendors/js/tables/datatable/responsive.bootstrap4.js"></script>
<script src="../app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js"></script>
<script src="../app-assets/vendors/js/tables/datatable/datatables.buttons.min.js"></script>
<script src="../app-assets/vendors/js/tables/datatable/jszip.min.js"></script>
<script src="../app-assets/vendors/js/tables/datatable/pdfmake.min.js"></script>
<script src="../app-assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
<script src="../app-assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
<script src="../app-assets/vendors/js/tables/datatable/buttons.print.min.js"></script>
<script src="../app-assets/vendors/js/tables/datatable/dataTables.rowGroup.min.js"></script>
<!-- END: Page JS-->

<script src="../assets/js/app.js"></script>

<script>
    $(window).on('load', function () {
        if (feather) {
            feather.replace({width: 14, height: 14});
        }
    })


    $("#create-user-form").submit(function (e) {
        e.preventDefault();
        const username = $("#user-user-username").val();
        const email = $("#user-user-email").val();
        const fullname = $("#user-user-fullname").val();
        const accessType = $("user-access-type").val();
        if (username.length < 1 || fullname.length < 1 || email.length < 1) {
            alert("Please fill required fields")
            return;
        }
        var formData = new FormData();
        formData.append("a", "reward");
        formData.append("b", userid);
        formData.append("c", amount);
        formData.append("d", description);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                var userObject = JSON.parse(xhttp.responseText);
                resp = userObject;

            }
        };
        xhttp.open("POST", this.apiURL);
        xhttp.send(formData);
    })


    function loadAllSystemUsers() {
        let formData = new FormData();
        formData.append("a", "getUsers");
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                let userObject = JSON.parse(xhttp.responseText);
                let resp = userObject;
                let dataSet = [];
                for (let ius = 0; ius < resp.length; ius++) {
                    let singleUser = resp[ius];
                    let tempds = [
                        singleUser.fullname,
                        singleUser.username,
                        singleUser.email,
                        "₦" + formatMoney(singleUser.ledger.income),
                        "₦" + formatMoney(singleUser.ledger.expense),
                        "₦" + formatMoney(singleUser.ledger.calculatedBalance),
                        singleUser.purse_id,
                        `<button class="btn btn-primary">Edit</button>`,
                        `<button class="btn btn-danger">Delete User</button>`
                    ];
                    dataSet.push(tempds);
                }
                $('#users-table').DataTable({
                    columnDefs: [{className: "control", orderable: !1, responsivePriority: 2, targets: 0}],
                    data: dataSet,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (e) {
                                    // console.log("_________________", e)
                                    return "Wallet User Details"
                                }
                            }),
                            type: "column",
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: "table",
                                // columnDefs: [{targets: 2, visible: !1}, {targets: 3, visible: !1}]
                            })
                        }
                    },
                    columns: [
                        {
                            title: "Full Name"
                        },
                        {
                            title: "Username"
                        },
                        {
                            title: "Email Address"
                        }, {
                            title: "Income"
                        }, {
                            title: "Expenses"
                        }, {
                            title: "Balance"
                        },
                        {
                            title: "Purse ID."
                        }, {
                            title: "Edit"
                        }, {
                            title: "Delete"
                        }
                    ]
                });
                $(".loadingOverlay").fadeOut();
            }
        };
        xhttp.onerror = function (e) {
            $(".fail-retry").fadeIn();
        }
        xhttp.open("POST", digiApiUrl);
        xhttp.send(formData);
    }

    loadAllSystemUsers();
</script>
</body>
</body>
</html>

