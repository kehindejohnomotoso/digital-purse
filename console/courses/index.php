<?php
session_start();
include_once "../assets/includeJS.php";


$selectDropDown = array();
$teachers = array();
if (!isset($_SESSION['uiLJKukykhHKVulvjhLKOUHcgVGHGfvhVGHchlv'])) {
    header("Location:../login/");
} else {
    $username = $_SESSION['uiLJKukykhHKVulvjhLKOUHcgVGHGfvhVGHchlv'];
    include_once "../../digitalpurse.php";
    $purse = new DigitalPurse();
    $runData = new stdClass();
    $selectDropDown = $purse->getInstitutionsAsSelect()->institutions;
    $teachers = $purse->getStakeholdersAsSelect()->teachers;
}

?>
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <meta name="author" content="Kehinde Omotoso">
    <meta name="author-email" content="kehindejohnomotoso@gmail.com">
    <title>Courses / Product - eWallet System</title>
    <link rel="apple-touch-icon" href="../app-assets/images/ico/apple-icon-120.html">
    <link rel="shortcut icon" type="image/x-icon"
          href="https://pixinvent.com/demo/vuexy-html-bootstrap-admin-template/../app-assets/images/ico/favicon.ico">
    <link
            href="../fonts.googleapis.com/css219a5.css?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600"
            rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/vendors.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="../app-assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/bootstrap-extended.min.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/colors.min.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/components.min.css">


    <link rel="stylesheet" type="text/css"
          href="../app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css"
          href="../app-assets/vendors/css/tables/datatable/responsive.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css">
    <link
            rel="stylesheet" type="text/css"
            href="../app-assets/vendors/css/tables/datatable/rowGroup.bootstrap4.min.css">


    <link rel="stylesheet" type="text/css"
          href="../app-assets/css/plugins/extensions/ext-component-sweet-alerts.min.css">

    <!-- BEGIN: Custom CSS-->
    <link
            rel="stylesheet" type="text/css" href="../assets/css/style.css"> <!-- END: Custom CSS-->

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->
<body
        class="vertical-layout vertical-menu-modern  navbar-floating footer-static  " data-open="click"
        data-menu="vertical-menu-modern" data-col="">

<!-- BEGIN: Header-->

<!-- END: Header-->


<!-- BEGIN: Main Menu-->
<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto">
                <a class="navbar-brand" href="">
                            <span class="brand-logo">
                                <svg viewbox="0 0 139 95" version="1.1" xmlns="http://www.w3.org/2000/svg"
                                     height="24">
                                    <defs>
                                        <lineargradient id="linearGradient-1" x1="100%" y1="10.5120544%" x2="50%"
                                                        y2="89.4879456%">
                                            <stop stop-color="#000000" offset="0%"></stop>
                                            <stop stop-color="#FFFFFF" offset="100%"></stop>
                                        </lineargradient>
                                        <lineargradient id="linearGradient-2" x1="64.0437835%" y1="46.3276743%"
                                                        x2="37.373316%" y2="100%">
                                            <stop stop-color="#EEEEEE" stop-opacity="0" offset="0%"></stop>
                                            <stop stop-color="#FFFFFF" offset="100%"></stop>
                                        </lineargradient>
                                    </defs>
                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <g id="Artboard" transform="translate(-400.000000, -178.000000)">
                                            <g id="Group" transform="translate(400.000000, 178.000000)">
                                                <path class="text-primary" id="Path"
                                                      d="M-5.68434189e-14,2.84217094e-14 L39.1816085,2.84217094e-14 L69.3453773,32.2519224 L101.428699,2.84217094e-14 L138.784583,2.84217094e-14 L138.784199,29.8015838 C137.958931,37.3510206 135.784352,42.5567762 132.260463,45.4188507 C128.736573,48.2809251 112.33867,64.5239941 83.0667527,94.1480575 L56.2750821,94.1480575 L6.71554594,44.4188507 C2.46876683,39.9813776 0.345377275,35.1089553 0.345377275,29.8015838 C0.345377275,24.4942122 0.230251516,14.560351 -5.68434189e-14,2.84217094e-14 Z"
                                                      style="fill:currentColor"></path>
                                                <path id="Path1"
                                                      d="M69.3453773,32.2519224 L101.428699,1.42108547e-14 L138.784583,1.42108547e-14 L138.784199,29.8015838 C137.958931,37.3510206 135.784352,42.5567762 132.260463,45.4188507 C128.736573,48.2809251 112.33867,64.5239941 83.0667527,94.1480575 L56.2750821,94.1480575 L32.8435758,70.5039241 L69.3453773,32.2519224 Z"
                                                      fill="url(#linearGradient-1)" opacity="0.2"></path>
                                                <polygon id="Path-2" fill="#000000" opacity="0.049999997"
                                                         points="69.3922914 32.4202615 32.8435758 70.5039241 54.0490008 16.1851325"></polygon>
                                                <polygon id="Path-21" fill="#000000" opacity="0.099999994"
                                                         points="69.3922914 32.4202615 32.8435758 70.5039241 58.3683556 20.7402338"></polygon>
                                                <polygon id="Path-3" fill="url(#linearGradient-2)" opacity="0.099999994"
                                                         points="101.428699 0 83.0667527 94.1480575 130.378721 47.0740288"></polygon>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                            </span>
                    <h2 class="brand-text">eWallet</h2>
                </a>
            </li>
        </ul>
    </div>
    <div class="shadow-bottom"></div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class=" navigation-header">
                <span data-i18n="Apps &amp; Pages">Application Menu</span>
                <i data-feather="more-horizontal"></i>
            </li>
            <li class="nav-item">
                <a class="d-flex align-items-center" href="../">
                    <i data-feather='activity'></i>
                    <span class="menu-title text-truncate" data-i18n="Overview">Overview</span>
                </a>
            </li>
            <li class=" nav-item">
                <a class="d-flex align-items-center" href="../users/">
                    <i data-feather='users'></i>
                    <span class="menu-title text-truncate" data-i18n="Overview">eWallet Users</span>
                </a>
            </li>
            <li class=" nav-item">
                <a class="d-flex align-items-center" href="../transactions/">
                    <i data-feather='percent'></i>
                    <span class="menu-title text-truncate" data-i18n="Overview">eWallet Transactions</span>
                </a>
            </li>
            <li class=" nav-item">
                <a class="d-flex align-items-center" href="../institution/">
                    <i data-feather='home'></i>
                    <span class="menu-title text-truncate" data-i18n="Overview">Institutions</span>
                </a>
            </li>
            <li class=" nav-item">
                <a class="d-flex align-items-center" href="../stakeholders/">
                    <i data-feather='user-check'></i>
                    <span class="menu-title text-truncate" data-i18n="Overview">eWallet Stakeholders</span>
                </a>
            </li>
            <li class=" nav-item sidebar-group-active">
                <a class="d-flex align-items-center" href="../courses/">
                    <i data-feather='book-open'></i>
                    <span class="menu-title text-truncate" data-i18n="Overview">Courses / Products</span>
                </a>
            </li>
            <!--            <li class=" nav-item">-->
            <!--                <a class="d-flex align-items-center" href="../withdrawal/">-->
            <!--                    <i data-feather='dollar-sign'></i>-->
            <!--                    <span class="menu-title text-truncate" data-i18n="Overview">Withdrawal Requests</span>-->
            <!--                </a>-->
            <!--            </li>-->
        </ul>
    </div>
</div>
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow">
        <div class="nav-flex-right">
            <div>
                <!--                <h6>Welcome, --><? //= $_SESSION['fn'] ?><!--</h6>-->
            </div>
            <div>
                <a class="d-flex align-items-center" href="../logout/">
                    <i data-feather='log-out'></i>
                    <span class="menu-title text-truncate" data-i18n="Overview">  &nbsp;Log Out</span>
                </a>
            </div>
        </div>
    </div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-12 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12 flex flex-row justify-content-between width-block">
                        <h2 class="content-header-title mb-0">
                            <i data-feather='home'></i>
                            eWallet Courses / Product</h2>
                        <button type="button" class="create-new btn btn-primary" data-toggle="modal"
                                data-target="#create-institution-modal">
                            <i data-feather='plus'></i>
                            Register Course
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div
                class="content-body">
            <!-- Dashboard Ecommerce Starts -->
            <section
                    id="dashboard-ecommerce">

                <!-- Basic table -->
                <section
                        id="dashboard-ecommerce">

                    <!-- Basic table -->
                    <section id="basic-datatable">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body card-datatable table-responsive pt-0">
                                        <table class="user-list-table table-hover-animation table-hover table"
                                               id="courses-table">

                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--/ Basic table -->

                    <div class="modal modal-slide-in fade" id="create-institution-modal">
                        <div class="modal-dialog sidebar-sm">
                            <form class="add-new-record modal-content pt-0" id="create-stakeholder-form">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
                                <div class="modal-header mb-1">
                                    <h5 class="modal-title" id="exampleModalLabel">Register Course</h5>
                                </div>
                                <div class="modal-body flex-grow-1">
                                    <div class="form-group">
                                        <label class="form-label" for="basic-icon-default-fullname">Course Name
                                            *</label>
                                        <input type="text" required class="form-control dt-full-name"
                                               id="course_name"
                                               placeholder=""/>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label" for="basic-icon-default-username">Course Code
                                            *</label>
                                        <input type="text" required class="form-control dt-full-name"
                                               id="course_code"
                                               placeholder=""/>
                                    </div>

                                    <div class="form-group custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="course_is_free">
                                        <label class="custom-control-label" for="course_is_free">Course is free
                                            access ?</label>
                                    </div>

                                    <div class="form-group hide-free-access">
                                        <label class="form-label" for="basic-icon-default-email">Amount
                                            *</label>
                                        <input type="number" id="course_amount" class="form-control dt-email"
                                               required/>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label" for="basic-icon-default-institution">Select
                                            Institution</label>
                                        <select class="form-control dt-access-type" required id="institution">
                                            <option value="">Please Select</option>
                                            <?php foreach ($selectDropDown as $option) { ?>
                                                <option value="<?= $option["institution_code"] ?>" <?= $option["deleted"] ? 'disabled' : '' ?>><?= $option["institution_name"] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-label" for="teacher">Select
                                            Teacher</label>
                                        <select class="form-control dt-access-type" required id="teacher">
                                            <option value="">Please Select</option>
                                        </select>
                                    </div>


                                    <div class="form-group custom-control custom-checkbox hide-free-access">
                                        <input type="checkbox" class="custom-control-input" id="course_is_fulltime">
                                        <label class="custom-control-label" for="course_is_fulltime">Course is full-time
                                            access only?</label>
                                    </div>

                                    <div class="form-group hide-non-filltime hide-free-access">
                                        <label class="form-label" for="subscription_type">Select
                                            Subscription Type</label>
                                        <select class="form-control dt-access-type" required id="subscription_type">
                                            <option value="">Please Select</option>
                                            <option value="D">Daily Subscription</option>
                                            <option value="W">Weekly Subscription</option>
                                            <option value="M">Monthly Subscription</option>
                                        </select>
                                    </div>

                                    <div class="form-group hide-non-filltime  hide-free-access">
                                        <label class="form-label" for="course_validity">Course Validity (Number
                                            of Days/Weeks/Months)
                                            *</label>
                                        <input type="number" id="course_validity" class="form-control dt-email"
                                               required/>
                                    </div>

                                    <button type="submit" class="btn btn-primary data-submit mr-1" id="create-button">
                                        Register Course
                                    </button>
                                    <button type="reset" class="btn btn-outline-secondary" data-dismiss="modal">Cancel
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>


                    <!--                    Update Modal BEGIN-->
                    <div class="modal modal-slide-in fade" id="update-modal">
                        <div class="modal-dialog sidebar-sm">
                            <form class="add-new-record modal-content pt-0" id="update-course-form">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
                                <div class="modal-header mb-1">
                                    <h5 class="modal-title" id="exampleModalLabel">Update Course Information</h5>
                                </div>
                                <div class="modal-body flex-grow-1">
                                    <div class="form-group">
                                        <label class="form-label" for="basic-icon-default-fullname">Course Name
                                            *</label>
                                        <input type="text" required class="form-control dt-full-name"
                                               id="upd_course_name"
                                               placeholder=""/>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label" for="basic-icon-default-username">Course Code
                                            *</label>
                                        <input type="text" disabled required class="form-control dt-full-name"
                                               id="upd_course_code"
                                               placeholder=""/>
                                    </div>

                                    <div class="form-group custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="upd_course_is_free">
                                        <label class="custom-control-label" for="course_is_free">Course is free
                                            access ?</label>
                                    </div>

                                    <div class="form-group hide-free-access">
                                        <label class="form-label" for="basic-icon-default-email">Amount
                                            *</label>
                                        <input type="number" id="upd_course_amount" class="form-control dt-email"
                                               required/>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label" for="basic-icon-default-institution">Select
                                            Institution *</label>
                                        <select class="form-control dt-access-type" required id="upd_institution">
                                            <option value="">Please Select</option>
                                            <?php foreach ($selectDropDown as $option) { ?>
                                                <option value="<?= $option["institution_code"] ?>" <?= $option["deleted"] === "1" ? 'disabled' : '' ?>><?= $option["institution_name"] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-label" for="teacher">Select
                                            Teacher *</label>
                                        <select class="form-control dt-access-type" required id="upd_teacher">
                                            <option value="">Please Select</option>
                                            <?php foreach ($teachers as $option) { ?>
                                                <option value="<?= $option["stakeholder_code"] ?>" <?= $option["disabled"] === "1" ? 'disabled' : '' ?>><?= $option["fullname"] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>


                                    <div class="form-group custom-control custom-checkbox hide-free-access">
                                        <input type="checkbox" class="custom-control-input" id="upd_course_is_fulltime">
                                        <label class="custom-control-label" for="course_is_fulltime">Course is full-time
                                            access only?</label>
                                    </div>

                                    <div class="form-group hide-non-filltime hide-free-access">
                                        <label class="form-label" for="subscription_type">Select
                                            Subscription Type</label>
                                        <select class="form-control dt-access-type" required id="upd_subscription_type">
                                            <option value="">Please Select</option>
                                            <option value="D">Daily Subscription</option>
                                            <option value="W">Weekly Subscription</option>
                                            <option value="M">Monthly Subscription</option>
                                        </select>
                                    </div>

                                    <div class="form-group hide-non-filltime  hide-free-access">
                                        <label class="form-label" for="course_validity">Course Validity (Number
                                            of Days/Weeks/Months)
                                            *</label>
                                        <input type="number" id="upd_course_validity" class="form-control dt-email"
                                               required/>
                                    </div>

                                    <button type="submit" class="btn btn-primary data-submit mr-1" id="update-button">
                                        Update Course
                                    </button>
                                    <button type="reset" class="btn btn-outline-secondary" data-dismiss="modal">Cancel
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!--                    Update Modal END-->


        </div>
    </div>
</div>
<!-- END: Content-->


<!-- BEGIN: Vendor JS-->
<script src="../app-assets/vendors/js/vendors.min.js"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
<script src="../app-assets/vendors/js/charts/apexcharts.min.js"></script>
<script src="../app-assets/vendors/js/extensions/toastr.min.js"></script>
<!-- <script src="../app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js"></script> -->
<!-- END: Page Vendor JS-->


<!-- BEGIN: Theme JS-->
<script src="../app-assets/js/core/app-menu.min.js"></script>
<script src="../app-assets/js/core/app.min.js"></script>
<!-- END: Theme JS-->


<!-- BEGIN: Page JS-->
<script src="../app-assets/js/scripts/charts/chart-apex.min.js"></script>


<script src="../app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js"></script>
<script src="../app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js"></script>
<script src="../app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js"></script>
<script src="../app-assets/vendors/js/tables/datatable/responsive.bootstrap4.js"></script>
<script src="../app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js"></script>
<script src="../app-assets/vendors/js/tables/datatable/datatables.buttons.min.js"></script>
<script src="../app-assets/vendors/js/tables/datatable/jszip.min.js"></script>
<script src="../app-assets/vendors/js/tables/datatable/pdfmake.min.js"></script>
<script src="../app-assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
<script src="../app-assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
<script src="../app-assets/vendors/js/tables/datatable/buttons.print.min.js"></script>
<script src="../app-assets/vendors/js/tables/datatable/dataTables.rowGroup.min.js"></script>
<!-- END: Page JS-->

<script src="../app-assets/vendors/js/extensions/sweetalert2.all.min.js"></script>

<script src="../assets/js/app.js"></script>

<script>
    $(window).on('load', function () {
        if (feather) {
            feather.replace({width: 14, height: 14});
        }
    })

    let allTeachers = <?= json_encode($teachers) ?>;
    window.allTeachers = allTeachers;

    $("#institution").change(function () {
        const institutionCode = $(this).val();
        $("#teacher").html("<option value=''>Please Select</option>");
        const filteredTeacher = window.allTeachers.filter((data) => {
            return data.institution_code === institutionCode;
        });
        filteredTeacher.map((data, index) => {
            $("#teacher").append(`<option value="${data.stakeholder_code}" ${data.disabled === "1" ? "disabled" : ""}">${data.fullname}</option>`);
        })
    });

    $("#course_is_fulltime").change(function () {
        const isFullTime = $(this).is(":checked");
        if (isFullTime) {
            $(".hide-non-filltime").fadeOut();
            $("#subscription_type").attr("required", false);
            $("#course_validity").attr("required", false);
        } else {
            $(".hide-non-filltime").fadeIn();
            $("#subscription_type").attr("required", true);
            $("#course_validity").attr("required", true);
        }
    });

    $("#course_is_free").change(function () {
        const isCourseFree = $(this).is(":checked");
        if (isCourseFree) {
            $(".hide-free-access").fadeOut();
            $("#course_amount").attr("required", false);
            $("#subscription_type").attr("required", false);
            $("#course_validity").attr("required", false);
        } else {
            $(".hide-free-access").fadeIn();
            $("#course_amount").attr("required", true);
            $("#subscription_type").attr("required", true);
            $("#course_validity").attr("required", true);
        }
    });


    $(document).ready(function () {
        $('#courses-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: digiApiUrl + "?a=getAllCourses",
            columnDefs: [{className: "control", orderable: true, responsivePriority: 2, targets: 0}],
            // data: dataSet,
            // responsive: {
            //     details: {
            //         display: $.fn.dataTable.Responsive.display.modal({
            //             header: function (e) {
            //                 // console.log("_________________", e)
            //                 return "Course Details"
            //             }
            //         }),
            //         type: "column",
            //         renderer: $.fn.dataTable.Responsive.renderer.tableAll({
            //             tableClass: "table",
            //             columnDefs: [{targets: 0, visible: true}]
            //         })
            //     }
            // },
            responsive: false,
            columns: [
                {
                    title: "Course Name"
                },
                {
                    title: "Course Code"
                },
                {
                    title: "Teacher"
                },
                {
                    title: "Institution"
                },
                {
                    title: "Subscription Validity"
                }, {
                    title: "Subscription Amount", render: function (data, type, row) {
                        return "₦" + formatMoney(data);
                    }
                }, {
                    title: "Subscription Type"
                },
                {
                    title: "Subscription Count", render: function (data, type, row) {
                        return data + " Subscription";
                    }
                },
                {
                    title: "Revenue Generated", render: function (data, type, row) {
                        return "₦" + formatMoney(data);
                    }
                },
                {
                    title: "Edit", render: function (data, type, row) {
                        return `<button class='btn btn-primary' data-data='${data}' onclick='editInstitution(this)'>Edit</button>`;
                    }
                },
                // {
                //     title: "Deactivate", render: function (data, type, row) {
                //         return `<button class="btn btn-danger" onclick='deleteInstitution(${data})'>Deactivate</button>`;
                //     }
                // }
            ]
        });
    })
    ;

    function editInstitution(e) {
        const selector = $(e);
        const rawData = $(selector).attr("data-data");
        const data = JSON.parse(rawData);
        console.log(data);
        $("#upd_course_name").val(data.product_name);
        $("#upd_course_amount").val(data.cost_in_real_money);
        $("#upd_course_code").val(data.product_code);
        $("#upd_institution").val(data.institution_code).attr("selected", "selected");
        $("#upd_teacher").val(data.stakeholder_code).attr("selected", "selected");
        if (data.only_fulltime_access === "0") {
            $("#upd_subscription_type").val(data.subscription_type.toUpperCase()).attr("selected", "selected");
            $("#upd_course_validity").val(data.subscription_validity_days);
        }
        $("#update-modal").modal("show");
    }

    function deleteInstitution(institutionId) {
        Swal.fire({
            icon: "warning",
            title: "Delete Institution",
            text: "Are you sure you want to delete this institution? No future transaction will be possible for this institution",
            showConfirmButton: true,
            confirmButtonText: "Confirm Delete",
            showCancelButton: true
        }).then((actionStatus) => {
            if (actionStatus.isConfirmed) {

            }
        })
    }


    $("#create-stakeholder-form").submit(function (e) {
        e.preventDefault();
        const stakeholder_code = $("#teacher").val();
        const course_name = $("#course_name").val();
        const course_code = $("#course_code").val();
        const amount = $("#course_amount").val();
        const institution_code = $("#institution").val();
        let subscription_type = $("#subscription_type").val();

        const courseIsFullTime = $("#course_is_fulltime").is(":checked") ? "Y" : "N";
        const courseIsFree = $("#course_is_free").is(":checked") ? "Y" : "N";
        let courseValidityFormat = 0;
        const perfectExpiryDay = 31;
        const perfectExpiryWeekDay = 7;


        let amountDay = 0;
        let amountWeek = 0;
        let amountMonth = 0;
        const validityInput = $("#course_validity").val();
        if (!$("#course_is_fulltime").is(":checked")) {
            switch ($("#subscription_type").val()) {
                case "D":
                    courseValidityFormat = validityInput;
                    amountDay = amount;
                    break;
                case "W":
                    courseValidityFormat = parseInt(validityInput) * perfectExpiryWeekDay;
                    amountWeek = amount;
                    break;
                case "M":
                    courseValidityFormat = parseInt(validityInput) * perfectExpiryDay;
                    amountMonth = amount;
                    break;
            }
        }

        if (subscription_type.length < 1) {
            subscription_type = "D";
        }


        $("#create-button").html("Processing...");
        let formData = new FormData();
        formData.append("a", "registerCourse");
        formData.append("b", institution_code);
        formData.append("c", stakeholder_code);
        formData.append("d", course_name);
        formData.append("e", course_code);
        formData.append("f", amount);
        formData.append("g", courseValidityFormat + "");
        formData.append("h", courseIsFullTime);
        formData.append("i", courseIsFree);
        formData.append("j", amountDay + "");
        formData.append("k", amountWeek + "");
        formData.append("l", amountMonth + "");
        formData.append("m", subscription_type + "");
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                let userObject = JSON.parse(xhttp.responseText);
                resp = userObject;
                if (resp.status === "success") {
                    Swal.fire({
                        icon: "success",
                        title: "Register Course",
                        text: resp.message,
                        timer: 1500
                    }).then((function () {
                        window.location.href = "";
                    }));

                } else {
                    Swal.fire({
                        icon: "error",
                        title: "Register Course",
                        text: resp.message,
                        timer: 1500
                    })
                    $("#create-button").html("Register Course");
                }
            }
        };
        xhttp.onerror = function () {
            $("#create-button").html("Register Course");
            Swal.fire({
                icon: "error",
                title: "Register Course",
                text: "Something went wrong, please try again.",
                timer: 1500
            })
        }
        xhttp.open("POST", digiApiUrl);
        xhttp.send(formData);
    });


    $("#update-course-form").submit(function (e) {
        e.preventDefault();
        const stakeholder_code = $("#upd_teacher").val();
        const course_name = $("#upd_course_name").val();
        const course_code = $("#upd_course_code").val();
        const amount = $("#upd_course_amount").val();
        const institution_code = $("#upd_institution").val();
        let subscription_type = $("#upd_subscription_type").val();

        const courseIsFullTime = $("#upd_course_is_fulltime").is(":checked") ? "Y" : "N";
        const courseIsFree = $("#upd_course_is_free").is(":checked") ? "Y" : "N";
        let courseValidityFormat = 0;
        const perfectExpiryDay = 31;
        const perfectExpiryWeekDay = 7;


        let amountDay = 0;
        let amountWeek = 0;
        let amountMonth = 0;
        const validityInput = $("#upd_course_validity").val();
        if (!$("#upd_course_is_fulltime").is(":checked")) {
            switch ($("#upd_subscription_type").val()) {
                case "D":
                    courseValidityFormat = validityInput;
                    amountDay = amount;
                    break;
                case "W":
                    courseValidityFormat = parseInt(validityInput) * perfectExpiryWeekDay;
                    amountWeek = amount;
                    break;
                case "M":
                    courseValidityFormat = parseInt(validityInput) * perfectExpiryDay;
                    amountMonth = amount;
                    break;
            }
        }

        if (subscription_type.length < 1) {
            subscription_type = "D";
        }


        $("#update-button").html("Processing...");
        let formData = new FormData();
        formData.append("a", "updateCourse");
        formData.append("b", institution_code);
        formData.append("c", stakeholder_code);
        formData.append("d", course_name);
        formData.append("e", course_code);
        formData.append("f", amount);
        formData.append("g", courseValidityFormat + "");
        formData.append("h", courseIsFullTime);
        formData.append("i", courseIsFree);
        formData.append("j", amountDay + "");
        formData.append("k", amountWeek + "");
        formData.append("l", amountMonth + "");
        formData.append("m", subscription_type + "");
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                let userObject = JSON.parse(xhttp.responseText);
                resp = userObject;
                if (resp.status === "success") {
                    Swal.fire({
                        icon: "success",
                        title: "Update Course Information",
                        text: resp.message,
                        timer: 1500
                    }).then((function () {
                        window.location.href = "";
                    }));

                } else {
                    Swal.fire({
                        icon: "error",
                        title: "Update Course Information",
                        text: resp.message,
                        timer: 1500
                    })
                    $("#update-button").html("Update Course");
                }
            }
        };
        xhttp.onerror = function () {
            $("#update-button").html("Update Course");
            Swal.fire({
                icon: "error",
                title: "Update Course Information",
                text: "Something went wrong, please try again.",
                timer: 1500
            })
        }
        xhttp.open("POST", digiApiUrl);
        xhttp.send(formData);
    });
</script>
</body>
</body>
</html>
