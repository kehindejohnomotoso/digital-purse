<?php
//session_start();

class DigitalPurse
{
    private $apiKey;
    private $digitalPurseUrl = "";
    private $isLoacallyHosted = false;
    private $dbType = 0;
    private $dbTypes = ["mysql", "sqlserver", "oracle"];
    private $dbname = "digitalpurse";
    private $dbhost = "localhost";
    private $db_username = "digitalpurse";
    private $db_password = "@@##admindigit";
    private $db_port = 3306;
    private $isError = false;
    private $errorMessage = "";
    private $connection = null;
    private $currency = "NGN";
    private $configured_domain_name = "localhost";
    private $default_email_address = "kehindejohnomotoso@gmail.com";
    private $pkey = "./76HJotGUO;KlkyktduioguioNKJioCYIhlGHVFYILbku45RVYg";

    public function __construct()
    {
//        if (($this->configured_domain_name !== $_SERVER["HTTP_HOST"]) && !$this->isLoacallyHosted) {
//            http_response_code(404);
//            die();
//        }
        if (!$this->isLoacallyHosted) {
            $this->dbname = "csdccons_dp";
            $this->dbhost = "64.20.63.204";
            $this->db_username = "csdccons_digitalpurse";
            $this->db_password = "@@##Digi01";
        }
        $this->connectToDatabase();
    }

    public function run($runData)
    {
        switch ($runData->a) {
            case "78GUH8jhbjhbUYG7":
                return $this->openPurse($runData->b, $runData->c);
            case "JHKY78978uhjiy87hl":
                return $this->getPurseID($runData->b);
            case "bHGUUjklloosedfhrDXfc":
                return $this->getBalance($runData->b, $runData->c);
            case "generatePassword":
                return $this->generatePassword($runData->b);
            case "I78hiGTYg76FYgug7oiuFTYIf":
                return $this->getOverviewData($runData->b);
            default:
                return new stdClass();
        }
    }

    public function runApi($runData)
    {
        switch ($runData->a) {
            case "userdata":
                return $this->getPurseDetailsByUserId($runData->b);
            case "logpayment":
                return $this->logPayment($runData);
            case "workpayment":
                return $this->processPayment($runData);
            case "checkbalance":
                return $this->getBalance($runData->b);
            case "adduser":
                return $this->openPurse($runData->b, $runData->c, $runData->d);
            case "transfer":
                return $this->transfer($runData->b, $runData->c, $runData->d);
            case "subscribeproduct":
                return $this->subscribeToProduct($runData->b, $runData->c, $runData->d, $runData->e, $runData->f);
            case "reward":
                return $this->reward($runData->b, $runData->c, $runData->d);
            case "ai":
                return $this->addInstitution($runData->b, $runData->c, $runData->d, $runData->e, $runData->f, $runData->g);
            case "uai":
                return $this->updateInstitution($runData->b, $runData->c, $runData->d, $runData->e, $runData->f, $runData->g);
            case "createStakeholder":
                return $this->addStakeholder($runData->b, $runData->c, $runData->d, $runData->e, $runData->f, $runData->g);
            case "updateStakeholder":
                return $this->updateStakeholder($runData->b, $runData->c, $runData->d, $runData->e, $runData->f, $runData->g);
            case "gai":
                return $this->getInstitutionsAsList();
            case "login":
                return $this->login($runData->b, $runData->c);
            case "sysdef":
                return $this->getSystemDefaults();
            case "getUsers":
                return $this->getPurseUsers();
            case "validateSubscription":
                return $this->validateSubscription($runData->b, $runData->c);
            case "userwallet":
                return $this->getPurseID($runData->b);
            case "subscriptions":
                return $this->getUserSubscriptions($runData->b, $runData->c, $runData->d, $runData->e);
            case "transactions":
                return $this->getUserTransactionDetails($runData->b, $runData->c, $runData->d, $runData->e);
            case "getCurrentCourseData":
                return $this->getCurrentCourseData($runData->b);
            case "registerCourse":
                return $this->registerCourse($runData);
            case "updateCourse":
                return $this->updateCourse($runData);
            case "systemTransactions":
                return $this->getAllTransactions($runData);
            case "getAllInstitutions":
                return $this->getAllInstitutions($runData);
            case "getAllStakeholders":
                return $this->getAllStakeholders($runData);
            case "getInstitutionsAsSelect":
                return $this->getInstitutionsAsSelect();
            case "getStakeholdersAsSelect":
                return $this->getStakeholdersAsSelect();
            case "getAllCourses":
                return $this->getAllCourses($runData);
            default:
                $resp = new stdClass();
                $resp->message = "ThbUYHB";
                return $resp;
        }
    }

    private function login($username, $password)
    {
        $response = new stdClass();
        $response->message = "Login Failed.";
        $response->status = "error";
        $response->code = 400;
        $response->username = $username;
        try {
            $do = $this->connection->prepare("SELECT * FROM system_users where username=?");
            $do->execute([$username]);
            $result = $do->fetch();
            if ($this->validatePassword($password, $result['password'])) {
                $_SESSION["fn"] = $result['name'];
                $response->message = "Login Successful.";
                $response->status = "success";
                $response->code = 200;
            }
//            $response->re = $result;
//            $response->gp = $this->generatePassword($password);
        } catch (Exception $e) {
            $response->message = "Login failed.";
            $response->status = "error";
        }
        return $response;
    }

    private function generatePassword($password)
    {
        return md5($password) . md5(rand(11111111, 999999999));
    }

    private function validatePassword($password, $dbpassword)
    {
        $realdbpass = substr($dbpassword, 0, 32);
        return md5($password) == $realdbpass;
    }


    private function logPayment($d)
    {
        $response = new stdClass();
        try {
            $do = $this->connection->prepare("INSERT INTO payment_log(amount,userid,purse_id,payment_channel,reference) VALUES(?,?,?,?,?)")->execute([$d->b, $d->c, $d->d, $d->e, $d->f]);
            $response->message = "Payment Logged";
            $response->status = "success";
        } catch (Exception $e) {
            $response->message = "Some error occoured while processing payment";
            $response->status = "error";
        }
        return $response;
    }

    private function processPayment($data)
    {
        $response = new stdClass();
        $this->connection->prepare("UPDATE payment_log SET message=?,status=?,transaction=? WHERE reference=?")->execute([$data->c, $data->d, $data->e, $data->b]);
        $transactionData = $this->getTransactionDetails($data->b);
        if ($data->d === "success") {
            $this->connection->prepare("INSERT INTO purse_chain(transaction_id,to_user_id,amount) VALUES(?,?,?)")->execute([$transactionData->transaction, $transactionData->userID, $transactionData->amount]);
            $this->connection->prepare("UPDATE purse SET balance=balance+$transactionData->amount WHERE purse_id=?")->execute([$transactionData->purseID]);
            $response->message = "Payment successful";
            $response->status = "success";
            $response->creditType = "credit";
        } else {
            $response->message = "Payment failed";
            $response->status = "success";
            $response->creditType = "failed";
        }
        return $response;
    }

    private function getTransactionDetails($reference)
    {
        $response = new stdClass();
        $do = $this->connection->prepare("SELECT purse_id,userid,amount,payment_channel,stampdate,status,message,transaction FROM payment_log where reference=?");
        $do->execute([$reference]);
        $result = $do->fetch();
        $response->purseID = $result["purse_id"];
        $response->userID = $result["userid"];
        $response->amount = $result["amount"];
        $response->payment_channel = $result["payment_channel"];
        $response->date = $result["stampdate"];
        $response->status = $result["status"];
        $response->message = $result["message"];
        $response->transaction = $result["transaction"];
        return $response;
    }

    private function getBalance($userid)
    {
        $response = new stdClass();
        $check = $this->connection->prepare("SELECT p.balance,p.purse_id,(select virtual_currency from purse_settings limit 0,1) currency,(select unit_per_value from purse_settings limit 0,1) unit_per_value,(select unit_method from purse_settings limit 0,1) unit_method FROM purse_users pu inner join purse p on pu.purse_id=p.purse_id WHERE pu.userid=?");
        $check->execute([$userid]);
        $result = $check->fetch();
        $balance = $result["balance"];
        $currency = $result["currency"];
        $response->message = "";
        $response->balance = $this->getVirtualBalance($result["balance"], $result["unit_per_value"], $result["unit_method"]);
        $response->currency = $currency;
        $response->userid = $userid;
        $response->purseid = $result["purse_id"];
        $response->rawBalance = $balance;
        $response->unitPerValue = $result["unit_per_value"];
        $response->status = "success";
        if (strlen($balance) < 1) {
            $response->message = "Purse owner $userid doesn't exist.";
            $response->status = "error";
        }
        return $response;
    }

    private function getTotalSpentUnspentWallet()
    {
        $response = new stdClass();
        $spent = $this->connection->prepare("SELECT SUM(amount)-(SELECT SUM(balance) FROM purse) total_spent FROM purse_chain WHERE transaction_type='refill'");
        $spent->execute();
        $spentResult = $spent->fetch(PDO::FETCH_ASSOC);

        ////
        $unspent = $this->connection->prepare("SELECT SUM(balance) total_unspent FROM purse");
        $unspent->execute();
        $unSpentResult = $unspent->fetch(PDO::FETCH_ASSOC);

        $response->spent = $spentResult["total_spent"];
        $response->unspent = $unSpentResult["total_unspent"];

        return $response;
    }

    private function transfer($from, $to, $amount, $toInstitution = false, $fromInstitution = false, $toStakeHolder = false, $fromStakeHolder = false)
    {
        $userFrom = $this->getPurseID($from);
        $userTo = $this->getPurseID($to);
        $response = new stdClass();
        if (strlen($userFrom->purseid) < 1) {
            $response->message = "User sending coin does not exist";
            $response->status = "error";
            return $response;
        }
        if (strlen($userTo->purseid) < 1) {
            $response->message = "User receiving coin does not exist";
            $response->status = "error";
            return $response;
        }
        $balanceFrom = $this->getBalance($from);
        $newAmount = $amount;
        if ($balanceFrom->rawBalance < $amount) {
            $response->message = "Insufficient Balance";
            $response->status = "error";
        } else {
            ////////////Receiver
            if ($toInstitution) {
                $this->connection->prepare("UPDATE digital_institutions SET balance=balance+$newAmount WHERE institution_code=?")->execute([$to]);
            } else if ($toStakeHolder) {
                $this->connection->prepare("UPDATE purse_institution_stakeholders SET balance=balance+$newAmount WHERE stakeholder_code=?")->execute([$to]);
            } else {
                $this->connection->prepare("UPDATE purse SET balance=balance+$newAmount WHERE purse_id=?")->execute([$userTo->purseid]);
            }

            ///////Sender
            if ($fromInstitution) {
                $this->connection->prepare("UPDATE digital_institutions SET balance=balance-$newAmount WHERE institution_code=?")->execute([$from]);
            } else if ($fromStakeHolder) {
                $this->connection->prepare("UPDATE purse_institution_stakeholders SET balance=balance-$newAmount WHERE stakeholder_code=?")->execute([$from]);
            } else {
                $this->connection->prepare("UPDATE purse SET balance=balance-$newAmount WHERE purse_id=?")->execute([$userFrom->purseid]);
            }
            ////
            $transactionId = $userFrom->purseid . "_" . $userTo->purseid;
            $this->connection->prepare("INSERT INTO purse_chain(purse_id,transaction_id,from_user_id,to_user_id,amount,transaction_type) VALUES(?,?,?,?,?,?)")->execute([$userFrom->purseid, $transactionId, $from, $to, $newAmount, "transfer"]);
            $response->status = "success";
            $response->message = "Transfer Successful";
        }
        return $response;
    }

    private function subscribeToProduct($userid, $product, $amount, $durationType, $duration)
    {
        $response = new stdClass();
        $userBalance = $this->getBalance($userid);
        $productData = $this->getProductConfiguration($product);
        $response->productData = $productData;
        if ($this->checkSubscriptionActive($userid, $product)) {
            $response->message = "Subscribed.";
            $response->status = "success";
            return $response;
        }
        if (!$productData->productData) {
            $response->message = "Course is not available for subscription.";
            $response->status = "error";
            return $response;
        }
        $realCost = $productData->productData["cost_in_real_money"];
        $lifetimeAccess = $productData->productData["only_fulltime_access"] === 1 || $productData->productData["only_fulltime_access"] === "1";
        $dr = 0;
        if (!$lifetimeAccess) {
            switch ($durationType) {
                case "Day":
                    $realCost = intval($duration) * intval($productData->productData["cost_per_day"]);
                    $dr = $duration;
                    break;
                case "Week":
                    $realCost = intval($duration) * intval($productData->productData["cost_per_week"]);
                    $dr = intval($duration) * 7;
                    break;
                case "Month":
                    $realCost = intval($duration) * intval($productData->productData["cost_per_month"]);
                    $dr = intval($duration) * 31;
                    break;
            }
        }
        if ($userBalance->rawBalance < $realCost) {
            $response->message = "Insufficient Balance.";
            $response->status = "error";
            return $response;
        }

        $response->message = "Subscribed.";
        $response->status = "success";
        $finalAmount = $realCost;
        $institutionAmount = $this->remitToInstitution($userid, $realCost, $productData->institutionData);
        $finalAmount = $finalAmount - $institutionAmount;
        if ($finalAmount < 1) {
            $finalAmount = 0;
        }
        $response->institutionAmount = $institutionAmount;
        $stakeholderAmount = $this->remitToStakeholder($userid, $realCost, $productData->stakeholderData);
        $finalAmount = $finalAmount - $stakeholderAmount;
        if ($finalAmount < 1) {
            $finalAmount = 0;
        }
        $response->stakeholderAmount = $stakeholderAmount;
        $response->finalCost = $finalAmount;
        $this->remitToSystem($userid, $finalAmount, $productData->productData);
        $this->saveProductSubscription($userid, $product, $finalAmount, $institutionAmount, $stakeholderAmount, $productData->stakeholderData["stakeholder_code"], $productData->institutionData["institution_code"], $dr);
        return $response;
    }

    private function remitToInstitution($userid, $amount, $instituteData)
    {
        $institutionAmount = 0;
        if ($instituteData) {
            if ($instituteData["share_to_institution"]) {
                $institutionAmount = ($instituteData["sharing_percentage"] / 100) * $amount;
                if ($institutionAmount > 0) {
                    $this->transfer($userid, $instituteData["institution_code"], $institutionAmount, true, false, false, false);
                }
            }
        }
        return $institutionAmount;
    }

    private function remitToStakeholder($userid, $amount, $stakeholderData)
    {
        $stakeholderAmount = 0;
        if ($stakeholderData) {
            if ($stakeholderData["share_to_stakeholder"]) {
                $stakeholderAmount = ($stakeholderData["sharing_percentage"] / 100) * $amount;
                if ($stakeholderAmount > 0) {
                    $this->transfer($userid, $stakeholderData["stakeholder_code"], $stakeholderAmount, false, false, true, false);
                }
            }
        }
        return $stakeholderAmount;
    }

    private function remitToSystem($userid, $amount, $productData)
    {
        $userPurse = $this->getPurseID($userid);
        $systemPurse = $this->getSystemUserData();
        $this->connection->prepare("UPDATE purse_system_user SET balance=balance+$amount")->execute();
        $this->connection->prepare("UPDATE purse SET balance=balance-$amount WHERE purse_id=?")->execute([$userPurse->purseid]);
        $transactionId = $userPurse->purseid . "_" . $systemPurse->purseid;
        $this->connection->prepare("INSERT INTO purse_chain(purse_id,transaction_id,from_user_id,to_user_id,amount,transaction_type,product_id) VALUES(?,?,?,?,?,?,?)")->execute([$userPurse->purseid, $transactionId, $userid, $systemPurse->userid, $amount, "subscription", $productData["product_code"] . "_" . $productData["product_name"]]);

    }

    private function saveProductSubscription($userid, $productCode, $systemAmount, $institutionAmount, $stakeholderAmount, $stakeholderCode, $institutionCode, $expiry_date)
    {
        $insert = $this->connection->prepare("INSERT INTO purse_subscriptions(product_code,institution_amount,stakeholder_amount,system_amount,user_id,stakeholder_code,institution_code,expiry_date) VALUES(?,?,?,?,?,?,?,?)");
        $insert->execute([$productCode, $institutionAmount, $stakeholderAmount, $systemAmount, $userid, $stakeholderCode, $institutionCode, $expiry_date]);
    }

    private function checkSubscriptionActive($userId, $productCode)
    {
        $check = $this->connection->prepare("SELECT COUNT(*) FROM purse_subscriptions WHERE subscription_status=? AND product_code=? AND user_id=?");
        $check->execute(["active", $productCode, $userId]);
        return $check->fetchColumn();
    }

    private function checkFirstSubscription($userId, $productCode)
    {
        $check = $this->connection->prepare("SELECT COUNT(*) FROM purse_subscriptions WHERE product_code=? AND user_id=?");
        $check->execute([$productCode, $userId]);
        return $check->fetchColumn();
    }

    private function getProductConfiguration($productId)
    {
        $response = new stdClass();
        $check = $this->connection->prepare("SELECT * FROM purse_products WHERE product_code=? LIMIT 0,1");
        $check->execute([$productId]);
        $productData = $check->fetch(PDO::FETCH_ASSOC);
        $response->productData = $productData;
        $check2 = $this->connection->prepare("SELECT * FROM digital_institutions WHERE institution_code=? LIMIT 0,1");
        $check2->execute([$productData["institution_code"]]);
        $institutionData = $check2->fetch(PDO::FETCH_ASSOC);
        $response->institutionData = $institutionData;
        $check3 = $this->connection->prepare("SELECT * FROM purse_institution_stakeholders WHERE stakeholder_code=? LIMIT 0,1");
        $check3->execute([$productData["stakeholder_code"]]);
        $stakeholderData = $check3->fetch(PDO::FETCH_ASSOC);
        $response->stakeholderData = $stakeholderData;
        return $response;
    }

    private function reward($userid, $amount, $description)
    {
        $response = new stdClass();
        $userPurse = $this->getPurseID($userid);
        $userBalance = $this->getBalance($userid);
        $systemPurse = $this->getSystemUserData();
        $newAmount = $userBalance->unitPerValue * $amount;
        if ($systemPurse->balance < $amount) {
            $response->message = "Insufficient System Balance";
            $response->status = "error";
        } else {
            try {
                $this->connection->prepare("UPDATE purse_system_user SET balance=balance-$newAmount")->execute();
                $this->connection->prepare("UPDATE purse SET balance=balance+$newAmount WHERE purse_id=?")->execute([$userPurse->purseid]);
                //
                $transactionid = $userPurse->purseid . "_" . $systemPurse->purseid;
                $this->connection->prepare("INSERT INTO purse_chain(purse_id,transaction_id,from_user_id,to_user_id,amount,transaction_type,product_id) VALUES(?,?,?,?,?,?,?)")->execute([$userPurse->purseid, $transactionid, $userid, $systemPurse->userid, $newAmount, "reward", $description]);
                $response->status = "success";
                $response->message = "User Rewarded";
            } catch (PDOException $e) {
                $response->message = $e->getMessage();
            }
        }
        return $response;
    }

    private function openPurse($userid, $fullname, $email)
    {
        $response = new stdClass();
        $purseExist = $this->isPurseOwnerExisting($userid, $email);
        $response->status = "error";
        if (!$purseExist) {
            try {
                $purseid = $this->generatePurseID($userid, $fullname);
                $this->connection->prepare("INSERT INTO purse_users(purse_id,userid,fullname,email) VALUES(?,?,?,?)")->execute([$purseid, $userid, $fullname, $email]);
                $this->connection->prepare("INSERT INTO purse(purse_id) VALUES(?)")->execute([$purseid]);
                $response->status = "success";
                $response->message = "Purse created.";
                $response->purseid = $purseid;
            } catch (PDOException $e) {
                echo "Error found " . $e->getMessage();
            }
        }
        $response->purseExist = $purseExist;
        if ($purseExist) {
            $response->message = "A purse owner already exist with this user id or email address";
        }
        return $response;
    }

    private function generatePurseID($userid, $fullname)
    {
        return md5(time() . "digitalpurse" . $userid . "" . $fullname);
    }

    private function withdrawl()
    {

    }

    private function closePurse()
    {

    }

    private function getPurseID($userid)
    {
        $response = new stdClass();
        $check = $this->connection->prepare("SELECT purse_id,fullname FROM purse_users WHERE userid=?");
        $check->execute([$userid]);
        $result = $check->fetch();
        $purseId = $result["purse_id"];
        $response->message = "";
        $response->purseid = $purseId;
        $response->userid = $userid;
        $response->fullname = $result["fullname"];
        $response->ledger = $this->getUserLedger($purseId);
        $response->status = "success";
        if (strlen($purseId) < 1) {
            $response->message = "Purse owner $userid doesn't exist.";
            $response->status = "error";
        }
        return $response;
    }

    private function getSystemUserData()
    {
        $response = new stdClass();
        $check = $this->connection->prepare("SELECT purse_id,userid,fullname,balance,(select virtual_currency from purse_settings limit 0,1) currency,(select unit_per_value from purse_settings limit 0,1) unit_per_value,(select unit_method from purse_settings limit 0,1) unit_method FROM purse_system_user");
        $check->execute();
        $result = $check->fetch();
        $response->message = "";
        $response->purseid = $result["purse_id"];
        $response->userid = $result["userid"];
        $response->fullname = $result["fullname"];
        $response->rawBalance = $result["balance"];
        $response->balance = $this->getVirtualBalance($result["balance"], $result["unit_per_value"], $result["unit_method"]);
        $response->status = "success";
        if (strlen($result["purse_id"]) < 1) {
            $response->message = "Purse owner doesn't exist.";
            $response->status = "error";
        }
        return $response;
    }

    private function getVirtualBalance($balance, $unitPerValue, $method)
    {
        switch ($method) {
            case 0: ///Do not apply virtual currency
                return $balance;
            case 1: ///Multiply by unit
                return $balance * $unitPerValue;;
            case 2: ///Divide by unit
                return $unitPerValue > 0 ? $balance / $unitPerValue : 0;;
        }
    }

    private function getSystemDefaults()
    {
        $response = new stdClass();
        $check = $this->connection->prepare("select * from purse_settings limit 0,1");
        $check->execute();
        $result = $check->fetch();
        $response->currency = $result["currency"];
        $response->unit_method = $result["unit_method"];
        $response->unit_per_value = $result["unit_per_value"];
        $response->virtual_currency = $result["virtual_currency"];
        $response->system_name = $result["system_name"];
        $response->wallet_active = $result["wallet_active"];
        $response->status = "success";
        return $response;
    }

    private function getPurseDetailsByUserId($userid)
    {
        $response = new stdClass();
        $check = $this->connection->prepare("SELECT purse_id,userid,fullname,email FROM purse_users WHERE userid=?");
        $check->execute([$userid]);
        $result = $check->fetch();
        $response->message = "Success";
        $response->purseID = $result["purse_id"];
        $response->userID = $result["userid"];
        $response->fullName = $result["fullname"];
        $response->email = strlen($result["email"]) > 0 ? $result["email"] : $this->default_email_address;
        if (strlen($response->purseID) < 1) {
            $response->message = "User does not exist.";
        }
        $response->userExist = strlen($response->purseID) > 0;
        return $response;
    }

    private function isPurseOwnerExisting($userid, $email)
    {
        $check = $this->connection->prepare("SELECT COUNT(*) CT FROM purse_users WHERE userid=? OR email=?");
        $check->execute([$userid, $email]);
        $result = $check->fetch();
        return $result["CT"] > 0;
    }

    private function connectToDatabase()
    {
        $selecteddb = $this->dbTypes[$this->dbType];
        try {
            switch ($selecteddb) {
                case "mysql":
                    $this->connection = new PDO("mysql:host=$this->dbhost;dbname=$this->dbname;port=$this->db_port", $this->db_username, $this->db_password);
                    break;
            }
            $this->isError = false;
            $this->errorMessage = "";
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $err) {
            $this->isError = true;
            $this->errorMessage = "Could not connect to the database $this->dbname because: " . $err->getMessage();
            var_dump($this->errorMessage);
        }
    }

    private function addInstitution($institutionName, $institutionCode, $institutionEmail, $percentage, $enableInstitution, $enableSharing)
    {
        $institutionName = strtoupper($institutionName);
        $response = new stdClass();
        if ($this->instituteExist($institutionName)) {
            $response->message = "Institution already exist";
            $response->status = "error";
            return $response;
        }
        if (strlen($institutionName) > 0) {
            $inst_code = $this->generateInstituteCode($institutionName);
            $response->instituteCode = $inst_code;
            $percentage = intval($percentage);
            $this->connection->prepare("INSERT INTO digital_institutions(institution_name,institution_code,email,sharing_percentage,active_flag,share_to_institution) VALUES(?,?,?,?,?,?)")->execute([$institutionName, $inst_code, $institutionEmail, $percentage, $enableInstitution, $enableSharing]);
            $response->message = "Institution Created with Institution Code: " . $response->instituteCode;
            $response->status = "success";
        } else {
            $response->message = "Institution name can not be blank.";
        }
        return $response;
    }

    private function updateInstitution($institutionName, $institutionCode, $institutionEmail, $percentage, $enableInstitution, $enableSharing)
    {
        $institutionName = strtoupper($institutionName);
        $response = new stdClass();
        if (strlen($institutionName) > 0) {
            $percentage = intval($percentage);
            $this->connection->prepare("UPDATE digital_institutions SET institution_name=?,email=?,sharing_percentage=?,share_to_institution=?,active_flag=? WHERE institution_code=?")->execute([$institutionName, $institutionEmail, $percentage, $enableSharing, $enableInstitution, $institutionCode]);
            $response->message = "Institution Updated successfully";
            $response->status = "success";
        } else {
            $response->message = "Something is not right, please try again.";
        }
        return $response;
    }

    private function instituteExist($institutionName)
    {
        $institutionName = strtoupper($institutionName);
        $check = $this->connection->prepare("SELECT COUNT(*) CT FROM digital_institutions WHERE institution_name=?");
        $check->execute([$institutionName]);
        $result = $check->fetch();
        return $result["CT"] > 0;
    }

    private function generateInstituteCode($in)
    {
        $code = rand(11111111, 99999999);
        $tin = "";
        $in = str_replace(" ", "", $in);
        for ($i = 0; $i < 4; $i++) {
            $tin .= $in[rand(0, strlen($in))];
        }
        return strtoupper($tin) . "" . $code;
    }


    private function addStakeholder($fullname, $username, $email, $sharing_percentage, $enableSharing, $institution_code)
    {
        $response = new stdClass();
        if ($this->StakeholderExist($username)) {
            $response->message = "Stakeholder / Teacher already exist";
            $response->status = "error";
            return $response;
        }
        try {
            $stakeholder_code = $this->generateInstituteCode($fullname . ' ' . $username);
            $response->stakeholdeCode = $stakeholder_code;
            $percentage = intval($sharing_percentage);
            $this->connection->prepare("INSERT INTO purse_institution_stakeholders(username, fullname, email, institution_code, sharing_percentage, stakeholder_code, share_to_stakeholder) 
            VALUES(?,?,?,?,?,?,?)")->execute([$username, $fullname, $email, $institution_code, $percentage, $stakeholder_code, $enableSharing]);
            $response->message = "Stakeholder Created Successfully";
            $response->status = "success";
        } catch (Exception $e) {
            $response->message = "Something went wrong, try again";
            $response->e = $e;
            $response->status = "error";
        }
        return $response;
    }

    private function updateStakeholder($fullname, $username, $email, $sharing_percentage, $enableSharing, $institution_code)
    {
        $response = new stdClass();
        if (strlen($fullname) < 1) {
            $response->message = "Full Name can not be empty.";
            $response->status = "error";
            return $response;
        }
        try {
            $percentage = intval($sharing_percentage);
            $this->connection->prepare("UPDATE purse_institution_stakeholders SET fullname=?,email=?,sharing_percentage=?,share_to_stakeholder=?,institution_code=? WHERE username=?")->execute([$fullname, $email, $percentage, $enableSharing, $institution_code, $username]);
            $response->message = "Stakeholder Updated Successfully";
            $response->status = "success";
        } catch (Exception $e) {
            $response->message = "Something went wrong, try again";
            $response->e = $e;
            $response->status = "error";
        }
        return $response;
    }

    private function StakeholderExist($username)
    {
        $check = $this->connection->prepare("SELECT COUNT(*) CT FROM purse_institution_stakeholders WHERE username=?");
        $check->execute([$username]);
        $result = $check->fetch();
        return $result["CT"] > 0;
    }


    private function getInstitutionsAsList()
    {
        $response = new stdClass();
        $check = $this->connection->prepare("SELECT institution_name,institution_code,sharing_percentage,active_flag,(SELECT COUNT(*) FROM purse_institution_stakeholders WHERE institution_code=DI.institution_code) stakeholders,(SELECT COUNT(*) FROM purse_products WHERE institution_code=DI.institution_code) products FROM digital_institutions DI");
        $check->execute();
        $result = $check->fetchAll(PDO::FETCH_ASSOC);
        $response->institutions = $result;
        $response->status = "success";
        $response->message = "Institution List";
        return $response;
    }

    public function getInstitutionsAsSelect()
    {
        $response = new stdClass();
        $response->institution = array();
        try {
            $check = $this->connection->prepare("SELECT institution_name,institution_code,deleted FROM digital_institutions");
            $check->execute();
            $result = $check->fetchAll(PDO::FETCH_ASSOC);
            $response->institutions = $result;
            $response->status = "success";
            $response->message = "Institution List";
        } catch (Exception $e) {

        }
        return $response;
    }

    public function getStakeholdersAsSelect()
    {
        $response = new stdClass();
        $response->institution = array();
        try {
            $check = $this->connection->prepare("SELECT stakeholder_code,institution_code,fullname,username,disabled FROM purse_institution_stakeholders");
            $check->execute();
            $result = $check->fetchAll(PDO::FETCH_ASSOC);
            $response->teachers = $result;
            $response->status = "success";
            $response->message = "Stakeholder List";
        } catch (Exception $e) {

        }
        return $response;
    }

    private function getUserLedger($purseId)
    {
        $response = new stdClass();
        $check = $this->connection->prepare("SELECT (SELECT SUM(AMOUNT) FROM purse_chain WHERE TO_USER_ID=PU.USERID) INCOME,(SELECT SUM(AMOUNT) FROM purse_chain WHERE FROM_USER_ID=PU.USERID) EXPENSE,(SELECT BALANCE FROM purse WHERE PURSE_ID=PU.PURSE_ID) SOLID_BALANCE FROM purse_chain PC,purse_users PU WHERE (PC.FROM_USER_ID=PU.USERID OR PC.TO_USER_ID=PU.USERID) AND PU.PURSE_ID=?");
        $check->execute([$purseId]);
        $result = $check->fetch();
        $response->income = $result["INCOME"] === null ? 0 : $result["INCOME"];
        $response->expense = $result["EXPENSE"] === null ? 0 : $result["EXPENSE"];
        $response->logBalance = $result["SOLID_BALANCE"];
        $response->calculatedBalance = $result["INCOME"] - $result["EXPENSE"];
        $response->fraudDetection = $response->logBalance != $response->calculatedBalance;
        return $response;
    }

    private function getPurseUsers()
    {
        $response = array();
        $check = $this->connection->prepare("SELECT PURSE_ID,USERID,FULLNAME,EMAIL,DATECREATED,(SELECT INSTITUTION_NAME FROM digital_institutions WHERE INSTITUTION_CODE=PU.INSTITUTION_ID) INSTITUTION_NAME FROM purse_users PU");
        $check->execute();
        $result = $check->fetchAll();
        foreach ($result as $user) {
            $us = new stdClass();
            $us->purse_id = $user["PURSE_ID"];
            $us->username = $user["USERID"];
            $us->fullname = $user["FULLNAME"];
            $us->email = $user["EMAIL"];
            $us->datecreated = $user["DATECREATED"];
            $us->ledger = $this->getUserLedger($user["PURSE_ID"]);

            array_push($response, $us);
        }
        return $response;
    }

    public function getApiDetails()
    {
        $details = new stdClass();
        $details->key = "pk_test_ee602786c4231870fe18023b2f6087bc9ba66df1";
        return json_encode($details);
    }

    private function normalizeSubscriptions()
    {
        $this->connection->prepare("UPDATE purse_subscriptions SET subscription_status='expired/inactive' WHERE (stampdate + INTERVAL expiry_date DAY < DATE(NOW())) AND expiry_date > 0")->execute();
        $this->connection->prepare("UPDATE purse_subscriptions SET subscription_status='active' WHERE (stampdate + INTERVAL expiry_date DAY > DATE(NOW())) OR expiry_date=0")->execute();
    }

    private function validateSubscription($userId, $productId)
    {
        $this->normalizeSubscriptions();
        $response = new stdClass();
        $response->status = $this->checkSubscriptionActive($userId, $productId) ? "active" : "expired";
        $response->firstTime = $this->checkFirstSubscription($userId, $productId);
        return $response;
    }

    private function getUserSubscriptions($userid, $draw = 1, $start = 0, $length = 10)
    {
        $this->normalizeSubscriptions();
        $systemVariables = $this->getSystemDefaults();
        $data = array();
        $response = new stdClass();
        $response->draw = $draw;
        $response->recordsTotal = $this->getTotalUserSubscriptions($userid);
        $response->recordsFiltered = $this->getTotalUserSubscriptions($userid);
        $check = $this->connection->prepare("SELECT *,(SELECT PRODUCT_NAME FROM purse_products WHERE PRODUCT_CODE=PS.PRODUCT_CODE) COURSE_NAME,(SELECT ADDDATE(PS.stampdate, INTERVAL (SELECT subscription_validity_days FROM purse_products WHERE product_code=PS.product_code) DAY)) EXPIRY_DATE,(SELECT institution_name FROM digital_institutions WHERE institution_code=PS.institution_code LIMIT 0,1) INSTITUTION_NAME,(SELECT fullname FROM purse_institution_stakeholders WHERE stakeholder_code=PS.stakeholder_code LIMIT 0,1) LECTURER_NAME FROM purse_subscriptions PS WHERE user_id=? ORDER BY id DESC LIMIT $start,$length");
        $check->execute([$userid]);
        $rs = $check->fetchAll(PDO::FETCH_ASSOC);
        foreach ($rs as $sub) {
            $courseName = $sub["COURSE_NAME"];
            $amountPaid = $systemVariables->currency . ($sub["institution_amount"] + $sub["stakeholder_amount"] + $sub["system_amount"]);
            $subscription_date = $sub["stampdate"];
            $expiry_date = $sub["EXPIRY_DATE"];
            $status = $sub["subscription_status"] === "active" ? "<span class='text text-success'>" . $sub["subscription_status"] . "</span>" : "<span class='text text-danger'>" . $sub["subscription_status"] . "</span>";
            $institution_name = $sub["INSTITUTION_NAME"];
            $stakeholder_name = $sub["LECTURER_NAME"];
            array_push($data, array($courseName, $amountPaid, $subscription_date, $expiry_date, $status, $institution_name, $stakeholder_name));
        }
        $response->data = $data;
        return $response;
    }

    private function getUserTransactionDetails($userid, $draw = 1, $start = 0, $length = 10)
    {
        $systemVariables = $this->getSystemDefaults();
        $data = array();
        $response = new stdClass();
        $response->draw = $draw;
        $response->recordsTotal = $this->getTotalUserTransactions($userid);
        $response->recordsFiltered = $this->getTotalUserTransactions($userid);
        $check = $this->connection->prepare("SELECT *,(SELECT fullname FROM purse_users WHERE userid=PC.from_user_id LIMIT 0,1) FROM_FULLNAME,(SELECT fullname FROM purse_users WHERE userid=PC.to_user_id LIMIT 0,1) TO_FULLNAME FROM purse_chain PC WHERE from_user_id=? OR to_user_id=? ORDER BY id DESC LIMIT $start,$length");
        $check->execute([$userid, $userid]);
        $res = $check->fetchAll(PDO::FETCH_ASSOC);
        foreach ($res as $trans) {
            $transactionDate = $trans["stampdate"];
            $transactionType = $trans["transaction_type"];
            $sender = $trans["FROM_FULLNAME"];
            $receiver = $trans["TO_FULLNAME"];
            if ($transactionType === "transfer") {
                if ($trans["from_user_id"] === $userid) {
                    $transactionType = "<span class='text text-danger'>" . $trans["transaction_type"] . " [expense]</span>";
                } else {
                    $transactionType = "<span class='text text-success'>" . $trans["transaction_type"] . " [income]</span>";
                }
            } else if ($transactionType === "refill") {
                $transactionType = "<span class='text text-success'>" . $trans["transaction_type"] . " [income]</span>";
                $sender = $systemVariables->system_name;
            } else if ($transactionType === "subscription") {
                $transactionType = "<span class='text text-danger'>" . $trans["transaction_type"] . " [expense]</span>";
                $receiver = $systemVariables->system_name;
            } else if ($transactionType === "reward") {
                $transactionType = "<span class='text text-success'>" . $trans["transaction_type"] . " [income]</span>";
                $sender = $systemVariables->system_name;
            }
            $transactionAmount = $systemVariables->currency . " " . $trans["amount"];
            $transactionReference = "DIGIP_" . $trans["id"];
            array_push($data, array($transactionDate, $transactionType, $transactionAmount, $sender, $receiver, $transactionReference));
        }
        $response->data = $data;
        return $response;
    }

    private function getTotalUserSubscriptions($userid)
    {
        $check = $this->connection->prepare("SELECT COUNT(*) FROM purse_subscriptions WHERE user_id=?");
        $check->execute([$userid]);
        return $check->fetchColumn();
    }

    private function getTotalUserTransactions($userid)
    {
        $check = $this->connection->prepare("SELECT COUNT(*) FROM purse_chain WHERE from_user_id=? OR to_user_id=?");
        $check->execute([$userid, $userid]);
        return $check->fetchColumn();
    }

    private function getCurrentCourseData($b)
    {
        $response = new stdClass();
        $check = $this->connection->prepare("SELECT product_name,cost_in_real_money,cost_per_day,cost_per_week,cost_per_month,only_fulltime_access FROM purse_products WHERE product_code=?");
        $check->execute([$b]);
        $result = $check->fetch(PDO::FETCH_ASSOC);
        if ($result) {
            $response->product_name = $result["product_name"];
            $response->life_time_cost = $result["cost_in_real_money"];
            $response->cost_per_day = $result["cost_per_day"];
            $response->cost_per_week = $result["cost_per_week"];
            $response->cost_per_month = $result["cost_per_month"];
            $response->only_fulltime_access = $result["only_fulltime_access"] === 1 || $result["only_fulltime_access"] === "1";
            $response->status = "success";
        } else {
            $response->status = "error";
        }
        return $response;
    }

    public function getOverviewData($b)
    {
        $response = new stdClass();
        ////Get Overview Data for console.
        $userData = $this->getAuthenticatedUserData($b);
        $statistics = $this->getStatisticsInformation($b, $userData->userType);
//        $fmt = new NumberFormatter('de_DE', NumberFormatter::DECIMAL);
//        $withdrawAbleAmount = $fmt->formatCurrency($this->getWithdrawAbleAmount($b, $userData->userType),"");
        $withdrawAbleAmount = $this->getWithdrawAbleAmount($b, $userData->userType);
        $totalWithdrawal = $this->getTotalWithdrawal($b, $userData->userType);

        $response->user = $userData;
        $response->stats = $statistics;
        $response->balance = $withdrawAbleAmount;
        $response->totalWithdrawal = $totalWithdrawal;

        $response->subscriptionStats = $this->getSubscriptionStats();

        $response->transactionBalance = $this->getTotalSpentUnspentWallet();

        return $response;
    }

    private function getFundsInCirculation()
    {

    }

    private function getSubscriptionStats()
    {
        $currentYear = $this->connection->prepare("SELECT institution_amount+stakeholder_amount+system_amount amount,MONTH(stampdate) subscription_month FROM purse_subscriptions WHERE YEAR(stampdate)=YEAR(CURDATE()) GROUP BY MONTH(stampdate)");
        $currentYear->execute();
        $currentYearResult = $currentYear->fetchAll(PDO::FETCH_ASSOC);

        $previousYear = $this->connection->prepare("SELECT institution_amount+stakeholder_amount+system_amount amount,MONTH(stampdate) subscription_month FROM purse_subscriptions WHERE YEAR(stampdate)=YEAR(DATE_SUB(CURDATE(), INTERVAL 1 YEAR)) GROUP BY MONTH(stampdate)");
        $previousYear->execute();
        $previousYearResult = $previousYear->fetchAll(PDO::FETCH_ASSOC);

        $response = new stdClass();
        $response->currentYear = $this->formatSubscriptionStatsToYear($currentYearResult);
        $response->previousYear = $this->formatSubscriptionStatsToYear($previousYearResult);
        return $response;

    }

    private function formatSubscriptionStatsToYear($statData)
    {
        $originalData = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        foreach ($statData as $stat) {
            $originalData[$stat["subscription_month"] - 1] = intval($stat["amount"]);
        }
        return $originalData;
    }

    private function getAuthenticatedUserData($username)
    {
        $response = new stdClass();
        $check = $this->connection->prepare("SELECT name,username,userType,stakeholderCode,email FROM system_users WHERE username=?");
        $check->execute([$username]);
        $result = $check->fetch(PDO::FETCH_ASSOC);
        if ($result) {
            $response->username = $username;
            $response->name = $result["name"];
            $response->userType = $result["userType"];
            $response->stakeholderCode = $result["stakeholderCode"];
            $response->email = $result["email"];
        }
        return $response;
    }

    private function getStatisticsInformation($institutionStakeholderCode, $userType)
    {
        $response = new stdClass();
        $check = null;
        $result = false;
        switch ($userType) {
            case "institution":
                $check = $this->connection->prepare("SELECT COUNT(*) stakeholders,(SELECT COUNT(*) FROM purse_products pp WHERE pp.institution_code=pis.institution_code) products FROM purse_institution_stakeholders pis WHERE pis.institution_code=?");
                $check->execute([$institutionStakeholderCode]);
                break;
            case "stakeholder":
                $check = $this->connection->prepare("SELECT COUNT(*) institutions, (SELECT COUNT(*) FROM purse_products WHERE stakeholder_code IN(SELECT stakeholder_code FROM purse_institution_stakeholders WHERE email=pis.email)) products FROM purse_institution_stakeholders pis WHERE email=?");
                $check->execute([$institutionStakeholderCode]);
                break;
            case "administrator" :
                $check = $this->connection->prepare("SELECT COUNT(*) institutions, (SELECT COUNT(DISTINCT product_code) FROM purse_products) products,(SELECT COUNT(DISTINCT email) FROM purse_institution_stakeholders) stakeholders FROM digital_institutions");
                $check->execute();
                break;
        }
        if ($check !== null) {
            $result = $check->fetch(PDO::FETCH_ASSOC);
        }
        if ($result) {
            $response->products = $result["products"];
            $response->stakeholders = $result["stakeholders"] !== null ? $result["stakeholders"] : 0;
            $response->institutions = $result["institutions"] !== null ? $result["institutions"] : 0;
        }
        return $response;
    }

    private function getWithdrawAbleAmount($institutionStakeholderCode, $userType)
    {
        $check = null;
        $result = null;
        $withdrawAbleAmount = 0;
        switch ($userType) {
            case "institution":
                $check = $this->connection->prepare("SELECT COALESCE(SUM(amount),0) total_income,(SELECT COALESCE(SUM(amount),0) FROM purse_chain WHERE from_user_id=pc.to_user_id AND pc.transaction_type NOT IN('withdrawal')) total_expense FROM purse_chain pc WHERE pc.to_user_id=?");
                $check->execute([$institutionStakeholderCode]);
                break;
            case "stakeholder":
                $check = $this->connection->prepare("SELECT COALESCE(SUM(amount),0) total_income,(SELECT COALESCE(SUM(amount),0) FROM purse_chain WHERE from_user_id=pc.to_user_id AND pc.transaction_type NOT IN('withdrawal')) total_expense FROM purse_chain pc WHERE pc.to_user_id IN(SELECT stakeholder_code FROM purse_institution_stakeholders WHERE email=?)");
                $check->execute([$institutionStakeholderCode]);
                break;
            case "administrator" :
                $check = $this->connection->prepare("SELECT COALESCE(SUM(amount),0) total_income,(SELECT COALESCE(SUM(amount),0) FROM purse_chain WHERE from_user_id=pc.to_user_id AND pc.transaction_type NOT IN('withdrawal')) total_expense FROM purse_chain pc WHERE pc.to_user_id IN(SELECT userid FROM purse_system_user)");
                $check->execute();
                break;
        }
        if ($check !== null) {
            $result = $check->fetch(PDO::FETCH_ASSOC);
        }
        if ($result !== null) {
            $withdrawAbleAmount = $result["total_income"] - $result["total_expense"];
        }
        return $withdrawAbleAmount;
    }

    private function getTotalWithdrawal($username, $userType)
    {
        $check = null;
        $result = null;
        $totalWithdrawal = 0;
        switch ($userType) {
            case 'institution':
                $check = $this->connection->prepare("SELECT COALESCE(SUM(amount),0) withdrawal FROM purse_chain pc WHERE pc.from_user_id=? AND pc.transaction_type='withdrawal'");
                $check->execute([$username]);
                break;
            case 'stakeholder':
                $check = $this->connection->prepare("SELECT COALESCE(SUM(amount),0) withdrawal FROM purse_chain pc WHERE pc.to_user_id IN(SELECT stakeholder_code FROM purse_institution_stakeholders WHERE email=?) AND pc.transaction_type = 'withdrawal'");
                $check->execute([$username]);
                break;
            case 'administrator':
                $check = $this->connection->prepare("SELECT COALESCE(SUM(amount),0) withdrawal FROM purse_chain pc WHERE pc.to_user_id IN(SELECT userid FROM purse_system_user) AND pc.transaction_type = 'withdrawal'");
                $check->execute();
                break;
        }
        if ($check !== null) {
            $result = $check->fetch(PDO::FETCH_ASSOC);
        }
        if ($result !== null) {
            $totalWithdrawal = $result["withdrawal"];
        }
        return $totalWithdrawal;
    }

    private function getActiveSubscriptions($username, $userTypes)
    {
        $activeSubscriptions = 0;
        $check = null;
        $result = null;
        switch ($userTypes) {
            case 'institution':
                $check = $this->connection->prepare("SELECT COALESCE(SUM(amount),0) withdrawal FROM purse_chain pc WHERE pc.from_user_id=? AND pc.transaction_type='withdrawal'");
                $check->execute([$username]);
                break;
            case 'stakeholder':
                $check = $this->connection->prepare("SELECT COALESCE(SUM(amount),0) withdrawal FROM purse_chain pc WHERE pc.to_user_id IN(SELECT stakeholder_code FROM purse_institution_stakeholders WHERE email=?) AND pc.transaction_type = 'withdrawal'");
                $check->execute([$username]);
                break;
            case 'administrator':
                $check = $this->connection->prepare("SELECT COUNT(*) active_subscriptions FROM purse_subscriptions WHERE subscription_status='active'");
                $check->execute();
                break;
        }
        if ($check !== null) {
            $result = $check->fetch(PDO::FETCH_ASSOC);
        }
        if ($result !== null) {
            $activeSubscriptions = $result["active_subscriptions"];
        }
        return $activeSubscriptions;
    }

    private function registerCourse($runData)
    {
        $response = new stdClass();
        $response->status = "error";
        $response->message = "could not add course";

        $institution_code = $runData->b;
        $stakeholder_code = $runData->c;
        $course_name = $runData->d;
        $course_code = $runData->e;
        $course_amount = $runData->f;
        $course_validity_days = $runData->g;
        $only_full_time_access = strtolower($runData->h);
        $free_access = $runData->i;
        $course_amount_day = $runData->j;
        $course_amount_week = $runData->k;
        $course_amount_month = $runData->l;
        $subscription_type = strtolower($runData->m);
        if ($this->checkCourseCodeExist($course_code)) {
            $response->message = "Duplicate course code. (parameter = e)";
            return $response;
        }
        if (strtolower($only_full_time_access) !== "y" && strtolower($only_full_time_access) !== "n") {
            $response->message = "Invalid full time access value specified. Valid types are (Y/N). (parameter = h)";
            return $response;
        } else if (strtolower($only_full_time_access) === "n" && $course_validity_days < 1) {
            $response->message = "Please specify course validity days for courses with no full time access. (parameter = g)";
            return $response;
        }
        if (strlen($course_name) < 1) {
            $response->message = "Please specify course name. (parameter = d)";
            return $response;
        }
        if (strlen($institution_code) < 1) {
            $response->message = "Please specify Institution Code. (parameter = b)";
            return $response;
        }
        if (strlen($stakeholder_code) < 1) {
            $response->message = "Please specify Stakeholder Code. (parameter = f)";
            return $response;
        }
        if ((strtolower($free_access) === "n" || strlen($free_access) < 1) && $course_amount < 1) {
            $response->message = "Please specify a course amount greater than 0. (parameter = f)";
            return $response;
        }

        if ($only_full_time_access === "n") {
            if (strtolower($subscription_type) !== "d" && strtolower($subscription_type) !== "w" && strtolower($subscription_type) !== "m") {
                $response->message = "Please specify course subscription type (d = days, w = weeks, m = month). (parameter = m)";
                return $response;
            }
            switch ($subscription_type) {
                case "d":
                    if ($course_amount_day < 0) {
                        $response->message = "You have specified that course is not only full time access. Because your subscription type is 'daily' (parameter = m) please specify course amount per day. (parameter = j)";
                        return $response;
                    }
                    break;
                case "w":
                    if ($course_amount_week < 0) {
                        $response->message = "You have specified that course is not only full time access. Because your subscription type is 'weekly' (parameter = m) please specify course amount per week. (parameter = k)";
                        return $response;
                    }
                    break;
                case "m":
                    if ($course_amount_month < 0) {
                        $response->message = "You have specified that course is not only full time access. Because your subscription type is 'monthly' (parameter = m) please specify course amount per month. (parameter = l)";
                        return $response;
                    }
                    break;
            }
        }
        $only_full_time_access = strtolower($only_full_time_access) == "n" ? 0 : 1;
        $check = $this->connection->prepare("INSERT INTO purse_products(institution_code,stakeholder_code,product_name,product_code,product_type,subscription_validity_days,cost_in_real_money,cost_per_day,cost_per_week,cost_per_month,only_fulltime_access,subscription_type) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)");
        $done = $check->execute([$institution_code, $stakeholder_code, $course_name, $course_code, "Course", $course_validity_days, $course_amount, $course_amount_day, $course_amount_week, $course_amount_month, $only_full_time_access, $subscription_type]);
        if ($done) {
            $response->message = "Course successfully added.";
            $response->status = "success";
        }
        return $response;
    }


    private function updateCourse($runData)
    {
        $response = new stdClass();
        $response->status = "error";
        $response->message = "could not add course";

        $institution_code = $runData->b;
        $stakeholder_code = $runData->c;
        $course_name = $runData->d;
        $course_code = $runData->e;
        $course_amount = $runData->f;
        $course_validity_days = $runData->g;
        $only_full_time_access = strtolower($runData->h);
        $free_access = $runData->i;
        $course_amount_day = $runData->j;
        $course_amount_week = $runData->k;
        $course_amount_month = $runData->l;
        $subscription_type = strtolower($runData->m);

        if (strtolower($only_full_time_access) !== "y" && strtolower($only_full_time_access) !== "n") {
            $response->message = "Invalid full time access value specified. Valid types are (Y/N). (parameter = h)";
            return $response;
        } else if (strtolower($only_full_time_access) === "n" && $course_validity_days < 1) {
            $response->message = "Please specify course validity days for courses with no full time access. (parameter = g)";
            return $response;
        }
        if (strlen($course_name) < 1) {
            $response->message = "Please specify course name. (parameter = d)";
            return $response;
        }
        if (strlen($institution_code) < 1) {
            $response->message = "Please specify Institution Code. (parameter = b)";
            return $response;
        }
        if (strlen($stakeholder_code) < 1) {
            $response->message = "Please specify Stakeholder Code. (parameter = f)";
            return $response;
        }
        if ((strtolower($free_access) === "n" || strlen($free_access) < 1) && $course_amount < 1) {
            $response->message = "Please specify a course amount greater than 0. (parameter = f)";
            return $response;
        }

        if ($only_full_time_access === "n") {
            if (strtolower($subscription_type) !== "d" && strtolower($subscription_type) !== "w" && strtolower($subscription_type) !== "m") {
                $response->message = "Please specify course subscription type (d = days, w = weeks, m = month). (parameter = m)";
                return $response;
            }
            switch ($subscription_type) {
                case "d":
                    if ($course_amount_day < 0) {
                        $response->message = "You have specified that course is not only full time access. Because your subscription type is 'daily' (parameter = m) please specify course amount per day. (parameter = j)";
                        return $response;
                    }
                    break;
                case "w":
                    if ($course_amount_week < 0) {
                        $response->message = "You have specified that course is not only full time access. Because your subscription type is 'weekly' (parameter = m) please specify course amount per week. (parameter = k)";
                        return $response;
                    }
                    break;
                case "m":
                    if ($course_amount_month < 0) {
                        $response->message = "You have specified that course is not only full time access. Because your subscription type is 'monthly' (parameter = m) please specify course amount per month. (parameter = l)";
                        return $response;
                    }
                    break;
            }
        }
        $only_full_time_access = strtolower($only_full_time_access) == "n" ? 0 : 1;
        $check = $this->connection->prepare("UPDATE purse_products SET product_name=?, cost_in_real_money=?, cost_per_day=?,cost_per_week=?,cost_per_month=?,institution_code=?,stakeholder_code=?,subscription_type=?,subscription_validity_days=?,only_fulltime_access=? WHERE product_code=?");
        $done = $check->execute([$course_name, $course_amount, $course_amount_day, $course_amount_week, $course_amount_month, $institution_code, $stakeholder_code, $subscription_type, $course_validity_days, $only_full_time_access, $course_code]);
        if ($done) {
            $response->message = "Course successfully added.";
            $response->status = "success";
        }
        return $response;
    }

    private function checkCourseCodeExist($course_code)
    {
        $check = $this->connection->prepare("SELECT COUNT(*) CT FROM purse_products WHERE product_code=?");
        $check->execute([$course_code]);
        $result = $check->fetch(PDO::FETCH_ASSOC);
        return $result["CT"] > 0;
    }

    private function getAllTransactions($runData)
    {
        $response = new stdClass();
        $response->data = array();
        $transactions = $this->connection->prepare("SELECT * FROM purse_chain ORDER BY stampdate DESC LIMIT $runData->d,$runData->e");
        $transactions->execute();
        $result = $transactions->fetchAll(PDO::FETCH_ASSOC);
        foreach ($result as $r) {
            $tempArray = array($r["from_user_id"], $r["to_user_id"], $r["amount"], $r["stampdate"], $r["product_id"], $r["transaction_type"], $r["transaction_id"], $r["purse_id"]);
            array_push($response->data, $tempArray);
        }
        $response->recordsFiltered = $this->getTransactionCount();
        $response->recordsTotal = $response->recordsFiltered;
        $response->draw = $runData->c;
        return $response;
    }

    private function getTransactionCount()
    {
        $count = $this->connection->prepare("SELECT COUNT(*) ct FROM purse_chain");
        $count->execute();
        $result = $count->fetch(PDO::FETCH_ASSOC);
        return $result["ct"];
    }

    private function getAllInstitutions($runData)
    {
        $response = new stdClass();
        $response->data = array();

        $institutions = $this->connection->prepare("SELECT id, institution_name, email, dash_display_name, institution_code, sharing_percentage, active_flag, is_public_display, share_to_institution, balance,(SELECT COUNT(*) FROM purse_institution_stakeholders WHERE DI.institution_code=institution_code) stakeholders,(SELECT COUNT(*) FROM purse_products WHERE DI.institution_code=institution_code) registered_products FROM digital_institutions DI WHERE NOT deleted LIMIT $runData->d,$runData->e");
        $institutions->execute();
        $result = $institutions->fetchAll(PDO::FETCH_ASSOC);
        foreach ($result as $r) {
            $tempArray = array($r["institution_name"], $r["institution_code"], $r["sharing_percentage"], $r["stakeholders"], $r["registered_products"], $r["balance"], $r["active_flag"], $r["share_to_institution"], json_encode($r), $r["id"]);
            array_push($response->data, $tempArray);
        }

        $response->recordsFiltered = $this->getInstitutionCount();
        $response->recordsTotal = $response->recordsFiltered;
        $response->draw = $runData->c;
        return $response;
    }

    private function getInstitutionCount()
    {
        $count = $this->connection->prepare("SELECT COUNT(*) ct FROM digital_institutions");
        $count->execute();
        $result = $count->fetch(PDO::FETCH_ASSOC);
        return $result["ct"];
    }


    private function getAllStakeholders($runData)
    {
        $response = new stdClass();
        $response->data = array();

        $institutions = $this->connection->prepare("SELECT id, username, email, fullname, institution_code, sharing_percentage, active_flag, balance,stakeholder_code,share_to_stakeholder, (SELECT institution_name FROM digital_institutions WHERE PIS.institution_code=institution_code) institution_name,(SELECT COUNT(*) FROM purse_products WHERE PIS.stakeholder_code=stakeholder_code) registered_products FROM purse_institution_stakeholders PIS LIMIT $runData->d,$runData->e");
        $institutions->execute();
        $result = $institutions->fetchAll(PDO::FETCH_ASSOC);
        foreach ($result as $r) {
            $tempArray = array($r["fullname"], $r["username"], $r["institution_name"], $r["email"], $r["sharing_percentage"], $r["registered_products"], $r["balance"], json_encode($r), $r["id"]);
            array_push($response->data, $tempArray);
        }

        $response->recordsFiltered = $this->getStakeholdersCount();
        $response->recordsTotal = $response->recordsFiltered;
        $response->draw = $runData->c;
        return $response;
    }

    private function getStakeholdersCount()
    {
        $count = $this->connection->prepare("SELECT COUNT(*) ct FROM purse_institution_stakeholders");
        $count->execute();
        $result = $count->fetch(PDO::FETCH_ASSOC);
        return $result["ct"];
    }

    private function getAllCourses($runData)
    {
        $response = new stdClass();
        $response->data = array();

        $institutions = $this->connection->prepare("SELECT id, institution_code, stakeholder_code, product_name, product_code, product_type, subscription_validity_days, cost_in_real_money, cost_per_day, cost_per_week, cost_per_month, only_fulltime_access, subscription_type,(SELECT institution_name FROM digital_institutions WHERE PIS.institution_code=institution_code) institution_name,(SELECT fullname FROM purse_institution_stakeholders WHERE PIS.stakeholder_code=stakeholder_code) stakeholder_name,(SELECT COUNT(*) FROM purse_subscriptions WHERE product_code=PIS.product_code) subscription_count,(SELECT SUM(institution_amount+system_amount+stakeholder_amount) FROM purse_subscriptions WHERE product_code=PIS.product_code) revenue_generated FROM purse_products PIS ORDER BY product_name LIMIT $runData->d,$runData->e");
        $institutions->execute();
        $result = $institutions->fetchAll(PDO::FETCH_ASSOC);
        foreach ($result as $r) {
            $validityDays = $r["only_fulltime_access"] ? "N/A" : $r["subscription_validity_days"];
            $subscriptionAmount = 0;
            $subscriptionType = "Lifetime";
            if ($r["only_fulltime_access"]) {
                $subscriptionAmount = $r["cost_in_real_money"];
            } else {
                switch (strtolower($r["subscription_type"])) {
                    case "d":
                        $subscriptionAmount = $r["cost_per_day"];
                        $subscriptionType = "Daily";
                        break;
                    case "m":
                        $subscriptionAmount = $r["cost_per_month"];
                        $subscriptionType = "Monthly";
                        break;
                    case "w":
                        $subscriptionAmount = $r["cost_per_week"];
                        $subscriptionType = "Weekly";
                        break;
                }
            }

            $tempArray = array($r["product_name"], $r["product_code"], $r["stakeholder_name"], $r["institution_name"], $validityDays, $subscriptionAmount, $subscriptionType, $r["subscription_count"], $r["revenue_generated"], json_encode($r), $r["id"]);
            array_push($response->data, $tempArray);
        }

        $response->recordsFiltered = $this->getCoursesCount();
        $response->recordsTotal = $response->recordsFiltered;
        $response->draw = $runData->c;
        return $response;
    }

    private function getCoursesCount()
    {
        $count = $this->connection->prepare("SELECT COUNT(*) ct FROM purse_products");
        $count->execute();
        $result = $count->fetch(PDO::FETCH_ASSOC);
        return $result["ct"];
    }

    public function getActiveSubscriptionCount()
    {
        $count = $this->connection->prepare("SELECT COUNT(*) ct FROM purse_subscriptions WHERE subscription_status='active'");
        $count->execute();
        $result = $count->fetch(PDO::FETCH_ASSOC);
        return $result["ct"];
    }

}