define([], function () {
    "use strict";

    function DigitalPurse(payThrough) {
        if (!(this instanceof DigitalPurse)) {
            console.error("Digital Purse was not properly innitialized...")
        }
        this.payThrough = payThrough;
        this.paymentChannel = "PAYSTACK";
        let purseInstance = this;
        this.locallyHosted = false;
        this.apiURL = this.locallyHosted ? "http://localhost/digitalpurse/digiapi.php" : "https://kennydigip.herokuapp.com/digiapi.php";
        if (payThrough) {
            this.paymentChannel = payThrough.toUpperCase();
        }
        console.log("DigitalPurse Initialized");
        if (this.paymentChannel === "PAYSTACK") {
            setTimeout(function () {
                purseInstance.loadPayStack();
            }, 5000);
        }

        if (window) {
            window.digitalpurse = this;
        } else {
            const checkWindowInterval = setInterval(function () {
                if (window) {
                    window.digitalpurse = purseInstance;
                    clearInterval(checkWindowInterval);
                    console.log("dp set to window", window.digitalpurse)
                } else {
                    console.log("Waiting for window event", window.digitalpurse)
                }
            }, 1000);
        }

        ///////Fire global event for other script to listen to, acknoleging availability of digital purse functions
        window.awaitingFunctions = [];
        if (document.createEvent) {
            const event = document.createEvent('Event');
            event.initEvent('digitalpurse_loaded', true, false);
            document.dispatchEvent(event);
        } else {
            console.error("Create Event Not Available, please enable javascript")
        }


        let datatablecss = document.createElement("link");
        datatablecss.type = "text/css";
        datatablecss.rel = "stylesheet";
        datatablecss.href = "https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css"
        document.head.appendChild(datatablecss);


        //Swal CSS
        let swalCSS = document.createElement("link");
        swalCSS.type = "text/css";
        swalCSS.rel = "stylesheet";
        swalCSS.href = "https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.16.6/sweetalert2.css";
        document.head.appendChild(swalCSS);
        ///////////Extra Dependencies ended

        let usersAllowedToWallet = [5];
        let userRole = null, isAStudent = false;
        if (window.digitalPurseSessionVariables) {
            isAStudent = window.digitalPurseSessionVariables.user.isStudent;
        }


        const getWalletDataIntervals = setInterval(function () {
            purseInstance.getUserWallet();
        }, 2000);
        this.getSystemDefaults();
        this.initAddWalletUser();
        this.initUpdateUserData();

        if (window) {
            window.digitalpurse = this;
        } else {
            const checkWindowInterval = setInterval(function () {
                if (window) {
                    window.digitalpurse = purseInstance;
                    clearInterval(checkWindowInterval);
                    console.log("dp set to window", window.digitalpurse)
                } else {
                    console.log("Waiting for window event", window.digitalpurse)
                }
            }, 1000);
        }
    }

    DigitalPurse.prototype = {
        constructor: DigitalPurse,

        loadPayStack: function () {
            let paystackScript = document.getElementById("digitalpurse_paystack_js");
            if (!paystackScript) {
                let script = document.createElement("script");
                script.type = "text/javascript";
                script.src = "https://js.paystack.co/v1/inline.js";
                script.id = "digitalpurse_paystack_js";
                document.head.appendChild(script);
            }
        },

        addCreateWalletButton: function () {
            if (!this.checkWalletButtonPresent("digitalpurse_open_wallet")) {
                ////////Add Open Wallet Button
                //Exclude Open Wallet for Admin user
                let usersAllowedToWallet = [5];
                let userRole = null, isAStudent = false;
                if (window.digitalPurseSessionVariables) {
                    isAStudent = window.digitalPurseSessionVariables.user.isStudent;
                }
                if (isAStudent) {
                    let owbc = document.createElement("div");
                    let owb = document.createElement("button");
                    owb.innerHTML = "<i class='far fa-credit-card'></i> ";
                    owb.appendChild(document.createTextNode("Open Wallet"));
                    owb.setAttribute("id", "digitalpurse_open_wallet");
                    owbc.setAttribute("style", "display:flex;align-items:center; position:relative;z-index:9999999;");
                    owb.setAttribute("style", "background:#ef476f !important;border-color:#ef476f !important;margin-right:2px;");
                    owb.classList.add("btn");
                    owb.classList.add("btn-primary");
                    owbc.appendChild(owb);
                    // document.body.appendChild(owbc);
                    $("span.username").parent().parent().attr("style", "display:flex;flex-direction:row;")
                    $("span.username").parent().parent().prepend(owbc);
                    let that = this;
                    owb.onclick = function (e) {
                        e.preventDefault();
                        that.openWallet();
                    }
                    console.log("Wallet Button has been added...")
                } else {
                    console.log("Wallet Button can not be added because this user is not a student...")
                    return;
                }
            } else {
                console.log("Wallet Button already added...")
            }
        },

        checkWalletButtonPresent: function (elementId) {
            let elementExist = false;
            let element = document.getElementById(elementId);
            if (typeof (element) != 'undefined' && element != null) {
                // Exists.
                elementExist = true;
            }
            return elementExist;
        },

        openWallet: function () {
            if (!window.digitalpurse.isWalletActive()) {
                Swal.fire({
                    icon: "error",
                    text: "Wallet features are not currently available, please contact administrator."
                });
                return;
            }
            const wd = window.digitalPurseUserWallet;
            const sysdef = window.digitalPurseSysDef;
            Swal.fire({
                title: "My Wallet",
                html: `
                <h4 style="font-weight: bolder;">${wd.fullname}</h4>
                <b id="digitalpurse_money_view">Balance: ${sysdef.currency}${wd.ledger.calculatedBalance}  (${window.digitalpurse.convertPriceToVirtualCurrency(wd.ledger.calculatedBalance).price}${sysdef.virtual_currency})</b>
                <br/>
                <div style="display: flex;justify-content: space-evenly;margin-top: 30px;margin-bottom:15px;align-content: center;">
                    <button class="btn" style="background: #0a9396 !important;color: #f9f9f9 !important;" onclick="window.digitalpurse.refill()">Refill My Wallet</button>
                    <button class="btn" onclick="window.digitalpurse.transfer()">Transfer From Wallet</button>
                </div>
                <div style="display: flex;justify-content: space-evenly;margin-bottom:10px;align-content: center;">
                    <button class="btn" onclick="window.digitalpurse.viewSubscriptions()">View Subscriptions</button>
                    <button class="btn" onclick="window.digitalpurse.viewTransactionHistory()">Transaction History</button>
                </div>
            `,
                showCloseButton: true,
                showCancelButton: false,
                showDenyButton: false,
                showConfirmButton: false
            });
        },

        pay: function (amount, userEmailAddress, userFullName, userID, purseID) {
            window.digitalpurse.loadPayStack();
            let resp;
            const that = this;
            const reference =
                purseID +
                "-" +
                userID +
                "-" +
                Math.floor(Math.random() * 1000000000 + 1);
            logPayment(amount, userID, purseID, reference);

            function payWithPaystack(amount, userEmailAddress, userID, purseID) {
                let paymentCredentials = getPaymentChannelCredentials();
                // console.log("REFERENCE", reference);
                let paystack = PaystackPop.setup({
                    key: paymentCredentials.key, // Replace with your public key
                    email: userEmailAddress,
                    amount: amount * 100,
                    ref: reference,
                    onClose: function () {
                        // alert("Window closed.");
                    },
                    callback: function (response) {
                        let message = "Payment complete! Reference: " + response.reference;
                        // console.log("Payment Response", response);
                        // alert(message);
                        updatePayment(reference, response.message, response.status, response.transaction, userID);
                    },
                });
                paystack.openIframe();
            }

            function payWithInterswitch() {
            }

            function payWithPaypal() {
            }

            function payWithRemita() {
            }

            function getPaymentChannelCredentials() {
                return {key: "pk_live_19fc84546b03a33a471b0a634a1827fc850dbc0b"};
            }

            function logPayment(amount, userID, purseID, reference) {
                let formData = new FormData();
                formData.append("a", "logpayment");
                formData.append("b", amount);
                formData.append("c", userID);
                formData.append("d", purseID);
                formData.append("e", that.paymentChannel);
                formData.append("f", reference);
                let xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function () {
                    if (xhttp.readyState === 4 && xhttp.status === 200) {
                        let res = JSON.parse(xhttp.responseText);
                        if (res.status === "success") {
                            switch (that.paymentChannel) {
                                case "PAYSTACK":
                                    payWithPaystack(amount, userEmailAddress, userID, purseID);
                                    break;
                            }
                        } else {
                            Swal.fire({
                                icon: 'error',
                                text: "Connection error occoured, please try again"
                            });
                        }
                    }
                };
                xhttp.open("POST", that.apiURL);
                xhttp.send(formData);
            }


            function updatePayment(reference, message, status, transaction, userid) {
                let formdata = new FormData();
                formdata.append("a", "workpayment");
                formdata.append("b", reference);
                formdata.append("c", message);
                formdata.append("d", status);
                formdata.append("e", transaction);
                formdata.append("f", userid);
                let xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function () {
                    if (xhttp.readyState === 4 && xhttp.status === 200) {
                        let res = JSON.parse(xhttp.responseText);
                        if (res.status === "success") {
                            if (res.creditType === "credit") {
                                // alert("Refil Successful");
                                that.getUserWallet();
                                Swal.fire({
                                    icon: "success",
                                    title: "Wallet Refill",
                                    text: "Wallet Refill Successful."
                                });
                            } else {
                                // alert("Refil faild");
                                Swal.fire({
                                    icon: "error",
                                    title: "Wallet Refill",
                                    text: "Wallet Refill Failed."
                                });
                            }
                        } else {
                            // alert("Connection error occoured, please try again");
                            Swal.fire({
                                icon: "error",
                                title: "Wallet Refill",
                                text: "Something went wrong, please try again."
                            });
                        }
                    }
                };
                xhttp.open("POST", that.apiURL);
                xhttp.send(formdata);
            }

            return resp;
        },

        viewSubscriptions: function () {
            let that = this;
            const userData = window.digitalpurse.getSessionData().user;
            Swal.fire({
                title: "My Subscriptions",
                width: 1200,
                html: `<table id="viewSubscription" class="display" style="width:100%;margin-top:20px;z-index:99999999999;">
                    <thead>
                        <tr>
                            <th>Course</th>
                            <th>Amount Paid</th>
                            <th>Subscription Date</th>
                            <th>Expiry Date</th>
                            <th>Status</th>
                            <th>Institution</th>
                            <th>Lecturer</th>
                        </tr>
                    </thead>
                </table>`,
                confirmButtonText: "Close",
                heightAuto: false
            });
            $('#viewSubscription').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": that.apiURL + "?a=subscriptions&b=" + userData.username,
                responsive: true,
            });
        },

        viewTransactionHistory: function () {
            let that = this;
            const userData = window.digitalpurse.getSessionData().user;
            Swal.fire({
                title: "Transaction History",
                width: 1200,
                html: `<table id="viewSubscription" class="display" style="width:100%;margin-top:20px;z-index:99999999999;">
                    <thead>
                        <tr>
                            <th>Transaction Date</th>
                            <th>Transaction Type</th>
                            <th>Transaction Amount</th>
                            <th>Sender</th>
                            <th>Receiver</th>
                            <th>Transaction Reference</th>
                        </tr>
                    </thead>
                </table>`,
                confirmButtonText: "Close",
                heightAuto: false
            });
            $('#viewSubscription').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": that.apiURL + "?a=transactions&b=" + userData.username,
                responsive: true
            });
        },

        getSystemDefaults: function () {
            let that = this;
            let formdata = new FormData();
            formdata.append("a", "sysdef");
            let xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (xhttp.readyState === 4 && xhttp.status === 200) {
                    let res = JSON.parse(xhttp.responseText);
                    window.digitalPurseSysDef = res;
                }
            };
            xhttp.open("POST", that.apiURL);
            xhttp.send(formdata);
        },

        getUserWallet: function () {
            let that = this;
            // console.log("Getting Wallet Data")
            const userData = window.digitalpurse.getSessionData().user;
            let getWUserInt = undefined;
            if (userData === undefined) {
                getWUserInt = setInterval(function () {
                    that.getUserWallet();
                }, 1000);
                return;
            } else {
                if (getWUserInt !== undefined) {
                    clearInterval(getWUserInt);
                }
            }
            const username = userData.username || "";
            let formData = new FormData();
            formData.append("a", "userwallet");
            formData.append("b", userData.username);
            const xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (xhttp.readyState === 4 && xhttp.status === 200) {
                    let res = JSON.parse(xhttp.responseText);
                    // console.log("WD R",res);
                    window.digitalPurseUserWallet = res;
                }
            };
            xhttp.open("POST", that.apiURL);
            xhttp.send(formData);
        },

        refill: function () {
            const userData = window.digitalpurse.getSessionData().user;
            const currency = window.digitalpurse.getSystemVariables().currency;
            const wallet = window.digitalPurseUserWallet;
            Swal.fire({
                title: 'Enter amount in ' + currency,
                input: 'number',
                showCancelButton: true,
                confirmButtonText: 'Pay Online',
                showLoaderOnConfirm: true,
                preConfirm: (amount) => {
                    window.digitalpurse.pay(amount, userData.email || "kehindejohnomotoso@gmail.com", userData.firstname + " " + userData.lastname, userData.username, wallet.purseid);
                },
                inputValidator: (amount) => {
                    return amount.length < 1 && 'Enter an amount greater than 0!'
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((res) => {
                window.digitalpurse.setSubscriptionPanelForOpening();
                if (window.goingForSubscription !== undefined) {
                    window.digitalpurse.validateSubscription()
                }
            })
        },

        getBalance: function () {
            const userid = window.digitalpurse.getSessionData().user.username || "";
            let resp;
            let that = this;
            let formData = new FormData();
            formData.append("a", "checkbalance");
            formData.append("b", userid);
            let xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState === 4 && this.status === 200) {
                    let userObject = JSON.parse(xhttp.responseText);
                    resp = userObject;

                    if (userObject.status === "success") {
                        alert("Your balance is: " + userObject.balance + " " + userObject.currency);
                    } else {
                        alert("user does not exist");
                    }
                }
            };
            xhttp.open("POST", window.digitalpurse.apiURL);
            xhttp.send(formData);
            return resp;
        },

        addWalletUser: function (userid, fullname, email) {
            let resp;
            let that = this;
            let formData = new FormData();
            formData.append("a", "adduser");
            formData.append("b", userid);
            formData.append("c", fullname);
            formData.append("d", email);
            let xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState === 4 && this.status === 200) {
                    let userObject = JSON.parse(xhttp.responseText);
                    resp = userObject;
                    if (userObject.status === "success") {
                        alert("User Added");
                    } else {
                        alert("Failed to add user because: " + userObject.message);
                    }
                }
            };
            xhttp.open("POST", window.digitalpurse.apiURL);
            xhttp.send(formData);
            return resp;
        },

        initAddWalletUser() {
            const userData = window.digitalpurse.getSessionData().user;
            let resp;
            let that = this;
            if (window.digitalpurse && (userData !== undefined)) {
                let formData = new FormData();
                formData.append("a", "adduser");
                formData.append("b", userData.username);
                formData.append("c", userData.firstname + " " + userData.lastname);
                formData.append("d", userData.email);
                let xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function () {
                    if (this.readyState === 4 && this.status === 200) {
                        let userObject = JSON.parse(xhttp.responseText);
                        resp = userObject;
                        if (userObject.status === "success") {
                            window.walletUserDataCreated = true;
                        }
                        // if (userObject.status === "success") {
                        //     alert("User Added");
                        // } else {
                        //     alert("Failed to add user because: " + userObject.message);
                        // }
                    }
                };
                xhttp.open("POST", window.digitalpurse.apiURL);
                xhttp.send(formData);
            } else {
                const creatUserInt = setInterval(function () {
                    that.initAddWalletUser();
                    if (window.walletUserDataCreated) {
                        clearInterval(creatUserInt);
                    }
                }, 1000);
            }
        },

        initUpdateUserData: function () {
            //////////
        },

        transfer: function () {
            let that = this;
            const userData = window.digitalpurse.getSessionData().user;
            const currency = window.digitalpurse.getSystemVariables().currency;
            const wallet = window.digitalPurseUserWallet;
            let transferTo = "";
            Swal.fire({
                title: 'Enter Receiver Username',
                input: 'text',
                showCancelButton: true,
                confirmButtonText: 'Continue',
                showLoaderOnConfirm: true,
                preConfirm: (username) => {
                    let formData = new FormData();
                    formData.append("a", "userwallet");
                    formData.append("b", username);
                    const xhttp = new XMLHttpRequest();
                    xhttp.onreadystatechange = function () {
                        if (xhttp.readyState === 4 && xhttp.status === 200) {
                            let res = JSON.parse(xhttp.responseText);
                            if (res.status === "success") {
                                // console.log("Running success swal")
                                transferTo = res.userid;
                                window.Swal.fire({
                                    title: 'Transfer to ' + res.fullname,
                                    text: 'Enter amount in ' + currency,
                                    input: "number",
                                    showCancelButton: true,
                                    showLoaderOnConfirm: true,
                                    preConfirm: (amount) => {
                                        if (amount) {
                                            let finalTransferFormData = new FormData();
                                            finalTransferFormData.append("a", "transfer");
                                            finalTransferFormData.append("b", wallet.userid);
                                            finalTransferFormData.append("c", transferTo);
                                            finalTransferFormData.append("d", amount);
                                            let xhttp = new XMLHttpRequest();
                                            xhttp.onreadystatechange = function () {
                                                if (this.readyState === 4 && this.status === 200) {
                                                    let userObject = JSON.parse(xhttp.responseText);
                                                    if (userObject.status === "success") {
                                                        window.Swal.fire({
                                                            icon: "success",
                                                            text: `Transfer amount of ${currency}${that.formatMoney(amount)} to ${res.fullname} was Successful`
                                                        });
                                                        that.getUserWallet();
                                                    } else {
                                                        window.Swal.fire({
                                                            icon: "error",
                                                            text: `Transfer amount of ${currency}${that.formatMoney(amount)} to ${res.fullname} has failed because ${userObject.message}`
                                                        });
                                                        that.getUserWallet();
                                                    }
                                                }
                                            };
                                            xhttp.open("POST", that.apiURL);
                                            xhttp.send(finalTransferFormData);
                                        }
                                    },
                                    inputValidator: (amount) => {
                                        return amount.length < 1 && 'Enter an amount greater than 0!'
                                    }
                                });
                            } else {
                                // console.log("Running fail swal")
                                window.Swal.fire({
                                    icon: "error",
                                    title: "Opps",
                                    text: "Purse user does not exist.",
                                    confirmButtonText: "Try Again",
                                    cancelButtonText: "Close",
                                    showCancelButton: true,
                                    showConfirmButton: true,
                                    preConfirm: (res) => {
                                        if (res) {
                                            that.transfer();
                                        }
                                    }
                                });
                            }
                        }
                    };
                    xhttp.open("POST", that.apiURL);
                    xhttp.send(formData);
                },
                inputValidator: (username) => {
                    return username.length < 1 && 'Enter username to continue!'
                },
                allowOutsideClick: () => !Swal.isLoading()
            })
        },

        formatMoney: function (amount, decimalCount = 2, decimal = ".", thousands = ",") {
            try {
                decimalCount = Math.abs(decimalCount);
                decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

                const negativeSign = amount < 0 ? "-" : "";

                let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
                let j = (i.length > 3) ? i.length % 3 : 0;

                return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
            } catch (e) {
                console.log(e)
            }
        },

        setSubscriptionPanelForOpening: function () {
            window.goingForSubscription = false;
            window.subscriptionSwalOpened = false;
        },

        subscribeToProduct: async function (target) {
            if (target !== undefined) {
                window.doNotShowCouldNotVerify = true;
            }
            if (!window.digitalpurse.isWalletActive()) {
                Swal.fire({
                    icon: "error",
                    text: "Wallet features are not currently available, please contact administrator."
                });
                window.digitalpurse.setSubscriptionPanelForOpening()
                if (window.goingForSubscription !== undefined) {
                    window.digitalpurse.validateSubscription();
                }
                return;
            }
            const courseData = window.courseToEnrollData;

            if (!courseData) {
                Swal.fire({
                    icon: "error",
                    text: "Course is not registered, please contact administrator."
                });
                window.location.href = "./";
                return;
            }

            let product, amount = courseData.life_time_cost, injectedMessage = "";
            if (Swal === undefined) {
                alert("Something went wrong, please refresh the page...")
                window.digitalpurse.setSubscriptionPanelForOpening()
                if (window.goingForSubscription !== undefined) {
                    window.digitalpurse.validateSubscription();
                }
                return;
            }
            if (courseData === undefined) {
                Swal.fire({
                    icon: "warning",
                    text: "Something went wrong, please ensure you have an internet connection and refresh the page."
                });
                window.digitalpurse.setSubscriptionPanelForOpening()
                if (window.goingForSubscription !== undefined) {
                    window.digitalpurse.validateSubscription();
                }
                return;
            }
            const subscriptionOption = {
                durationType: "",
                duration: 0
            }
            let swalConfirmed = false;
            if (!courseData.only_fulltime_access) {
                const {value: cs} = await Swal.fire({
                    title: 'Select Subscription option.',
                    html: `
                    <select id="subscription-type" class="swal2-input" required>
                        <option value="">Select Subscription Type</option>
                        <option value="Day">Day(s)</option>
                        <option value="Week">Week(s)</option>
                        <option value="Month">Month(s)</option>
<!--                        <option value="lifetime">Lifetime</option>-->
                    </select>
                    <input type="number" required class="swal2-input" id="subscription-duration" placeholder="Subscription Duration" style="display: block;width: 100% !important;min-width: -webkit-fill-available;">
                `,
                    showCancelButton: true,
                    preConfirm: () => {
                        const subType = $("#subscription-type").val(), subDuration = $("#subscription-duration").val();
                        swalConfirmed = true;
                        return [subType, subDuration]
                    },
                });
                if (!swalConfirmed) {
                    window.digitalpurse.setSubscriptionPanelForOpening()
                    if (window.goingForSubscription !== undefined) {
                        window.digitalpurse.validateSubscription();
                    }
                    return;
                }
                if (cs) {
                    subscriptionOption.durationType = cs[0];
                    subscriptionOption.duration = cs[1];
                }
                if (subscriptionOption.durationType.length < 1) {
                    Swal.fire({
                        icon: "error",
                        text: "Please select a subscription type to continue"
                    });
                    window.digitalpurse.setSubscriptionPanelForOpening()
                    if (window.goingForSubscription !== undefined) {
                        window.digitalpurse.validateSubscription();
                    }
                    return;
                }
                if (subscriptionOption.durationType !== "lifetime" && subscriptionOption.duration < 1) {
                    Swal.fire({
                        icon: "error",
                        text: "Please choose a duration greater than zero."
                    });
                    window.digitalpurse.setSubscriptionPanelForOpening()
                    if (window.goingForSubscription !== undefined) {
                        window.digitalpurse.validateSubscription();
                    }
                    return;
                }
                if (subscriptionOption.durationType !== "lifetime") {
                    injectedMessage = " for " + subscriptionOption.duration + " " + subscriptionOption.durationType + "(s) will";
                    switch (subscriptionOption.durationType) {
                        case "Day":
                            amount = courseData.cost_per_day * parseInt(subscriptionOption.duration);
                            break;
                        case "Week":
                            amount = courseData.cost_per_week * parseInt(subscriptionOption.duration);
                            break;
                        case "Month":
                            amount = courseData.cost_per_month * parseInt(subscriptionOption.duration);
                            break;
                    }
                }
            }
            const currentCourse = window.currentCourseToEnrol.currentCourse;

            const userid = window.digitalpurse.getSessionData().user.username || "";
            if (currentCourse) {
                product = currentCourse.idnumber;
                console.log("Subscription Amount", amount)
                // if (amount === 0) {
                //     amount = this.getOnlyNumberFromString(this.getValueFromCustomField(currentCourse.customFields, "price"));
                // }
            } else {
                alert("Invalid course")
                window.digitalpurse.setSubscriptionPanelForOpening()
                if (window.goingForSubscription !== undefined) {
                    window.digitalpurse.validateSubscription();
                }
                return;
            }

            if (amount < 1) {
                Swal.fire({
                    icon: "error",
                    text: "Something went wrong, contact administrator"
                });
                window.digitalpurse.setSubscriptionPanelForOpening()
                if (window.goingForSubscription !== undefined) {
                    window.digitalpurse.validateSubscription();
                }
                return;
            }

            let resp;
            let that = this;

            Swal.fire({
                title: 'Subscribe to Course',
                html: "Subscription to " + currentCourse.fullname + injectedMessage + " cost " + window.digitalpurse.getSystemVariables().currency + "" + window.digitalpurse.formatMoney(amount) + " [" + window.digitalpurse.formatMoney(window.digitalpurse.convertPriceToVirtualCurrency(amount).price) + " " + window.digitalpurse.convertPriceToVirtualCurrency(amount).currency + "]. Subscription amount will be deducted from your wallet, continue?",
                icon: 'info',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Subscribe!'
            }).then((result) => {
                if (result.isConfirmed) {
                    let formData = new FormData();
                    formData.append("a", "subscribeproduct");
                    formData.append("b", userid);
                    formData.append("c", product);
                    formData.append("d", amount);
                    formData.append("e", subscriptionOption.durationType);
                    formData.append("f", subscriptionOption.duration);
                    let xhttp = new XMLHttpRequest();
                    window.subscriptionSwalOpened = true;
                    xhttp.onreadystatechange = function () {
                        if (this.readyState === 4 && this.status === 200) {
                            let userObject = JSON.parse(xhttp.responseText);
                            resp = userObject;
                            if (userObject.status === "success") {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Subscription Successful',
                                    text: "Your subscription to " + currentCourse.fullname + " was successful",
                                    confirmButtonColor: '#30d6b5',
                                    confirmButtonText: 'Open Course'
                                }).then((res) => {
                                    if (target) {
                                        target.closest("form").submit();
                                    } else {
                                        if (window.linkToLoad) {
                                            window.location.href = linkToLoad;
                                        }
                                    }
                                });
                            } else {
                                // alert("Failed to subscribe because: " + userObject.message);
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Subscription Failed',
                                    text: userObject.message,
                                    footer: '<a href="#" onclick="window.digitalpurse.refill()">Refill Wallet</a>'
                                }).then((res) => {
                                    // window.digitalpurse.setSubscriptionPanelForOpening();
                                    // if (window.goingForSubscription !== undefined) {
                                    //     window.digitalpurse.validateSubscription()
                                    // }
                                })

                            }
                        }
                    };
                    xhttp.open("POST", window.digitalpurse.apiURL);
                    xhttp.send(formData);
                }
                window.digitalpurse.setSubscriptionPanelForOpening()
                if (window.goingForSubscription !== undefined) {
                    window.digitalpurse.validateSubscription();
                }
            });
            return resp;
        },

        validateSubscription: function () {
            window.goingForSubscription = false;
            const userData = window.currentCourse;
            const userRole = window.digitalPurseSessionVariables.user.rolename;
            if (userRole !== "student") {
                return;
            }
            if (userData) {
                if ((goingForSubscription && window.subscriptionExhausted) || window.subscriptionSwalOpened) {
                    return;
                }
                window.subscriptionSwalOpened = true;
                let formData = new FormData();
                formData.append("a", "validateSubscription");
                formData.append("b", userData.user.username);
                formData.append("c", userData.currentCourse.idnumber);
                let xmlHttp = new XMLHttpRequest();
                xmlHttp.onreadystatechange = function () {
                    if (this.readyState === 4 && this.status === 200) {
                        let userObject = JSON.parse(xmlHttp.responseText);
                        if (userObject.status === "active") {
                            // console.log("Subscription is active")
                        } else {
                            let message = `You have not subscribed to  ${userData.currentCourse.fullname}. Please subscribe now.`;
                            if (!userObject.firstTime) {
                                message = `Your subscription to ${userData.currentCourse.fullname} has expired.`;
                            }
                            window.subscriptionExhausted = true;
                            console.warn("Subscription to course has expired")
                            // window.subscriptionPrompte
                            Swal.fire({
                                icon: 'error',
                                title: 'Subscription Expired',
                                showConfirmButton: true,
                                confirmButtonText: "Subscribe now",
                                showCancelButton: true,
                                cancelButtonText: "Close",
                                text: message,
                                preConfirm: (confirm) => {
                                    if (confirm) {
                                        window.linkToLoad = "";
                                        window.goingForSubscription = true;
                                        window.digitalpurse.subscribeToProduct();
                                    }
                                }
                            }).then((data) => {
                                if (!window.goingForSubscription) {
                                    window.location.href = "./";
                                }
                                window.subscriptionSwalOpened = false;
                            });
                        }
                    }
                };
                xmlHttp.open("POST", window.digitalpurse.apiURL);
                xmlHttp.send(formData);
            } else {
                window.subscriptionExhausted = true;
                if (window.doNotShowCouldNotVerify === undefined) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Opps',
                        text: "Can not verify subscription.",
                    }).then((data) => {
                        window.location.href = "./";
                    });
                }
            }
        },

        getValueFromCustomField: function (customFields, label) {
            let value = "";
            if (customFields && label) {
                loopLabel : for (let ifields = 0; ifields < customFields.length; ifields++) {
                    let currF = customFields[ifields];
                    if (currF.shortname === label.toLowerCase()) {
                        value = currF.value;
                        break loopLabel;
                    }
                }
            }
            return value;
        },

        getOnlyNumberFromString: function (numberString) {
            numberString = numberString + "";
            return numberString.replace(/[^\d\.]*/g, '');
        },

        convertPriceToVirtualCurrency: function (price) {
            price = window.digitalpurse.getOnlyNumberFromString(price);
            const digiSysDef = window.digitalPurseSysDef;
            let virtualPrice = 0;
            if (digiSysDef) {
                switch (parseInt(digiSysDef.unit_method)) {
                    case 0: //Do not apply virtual currency
                        virtualPrice = price;
                        break;
                    case 1: //Multiply by unit
                        virtualPrice = price * parseInt(digiSysDef.unit_per_value);
                        break;
                    case 2: //Divide by unit
                        virtualPrice = parseInt(digiSysDef.unit_per_value) > 0 ? price / parseInt(digiSysDef.unit_per_value) : price;
                        break;
                }
            }
            return {
                price: virtualPrice,
                price_currency: virtualPrice + " " + digiSysDef.virtual_currency,
                currency: digiSysDef.virtual_currency
            };
        },

        getSystemVariables: function () {
            return window.digitalPurseSysDef;
        },

        rewardUser: function (userid, amount, description) {
            let resp;
            let that = this;
            let formData = new FormData();
            formData.append("a", "reward");
            formData.append("b", userid);
            formData.append("c", amount);
            formData.append("d", description);
            let xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState === 4 && this.status === 200) {
                    let userObject = JSON.parse(xhttp.responseText);
                    resp = userObject;
                    if (userObject.status === "success") {
                        alert("Reward Successful.");
                    } else {
                        alert("Failed to reward because: " + userObject.message);
                    }
                }
            };
            xhttp.open("POST", window.digitalpurse.apiURL);
            xhttp.send(formData);
            return resp;
        },

        addInstitution: function (institutionName, percentage) {
            let resp;
            let that = this;
            let formData = new FormData();
            formData.append("a", "ai");
            formData.append("b", institutionName);
            formData.append("c", percentage);
            let xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState === 4 && this.status === 200) {
                    let userObject = JSON.parse(xhttp.responseText);
                    resp = userObject;
                    if (userObject.status === "success") {
                        alert("Institution Added.");
                    } else {
                        alert("Failed to add institution because: " + userObject.message);
                    }
                }
            };
            xhttp.open("POST", window.digitalpurse.apiURL);
            xhttp.send(formData);
            return resp;
        },

        getInstitutionAsList: function () {
            let resp;
            let that = this;
            let formData = new FormData();
            formData.append("a", "gai");
            let xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState === 4 && this.status === 200) {
                    let userObject = JSON.parse(xhttp.responseText);
                    resp = userObject;
                    // console.log("Institutions", resp)
                    if (userObject.status === "success") {
                        // alert("Institutions Retrieved.");
                    } else {
                        // alert("Failed to add institution because: " + userObject.message);
                    }
                }
            };
            xhttp.open("POST", window.digitalpurse.apiURL);
            xhttp.send(formData);
            return resp;
        },

        getSessionData: function () {
            if (window && window.digitalPurseSessionVariables) {
                return window.digitalPurseSessionVariables
            } else {
                return {}
            }
        },

        getCourseToEnrollData: function () {
            console.log("Getting data for course to enroll from database")
            const cCTE = window.currentCourseToEnrol.currentCourse.idnumber;
            let formData = new FormData();
            formData.append("a", "getCurrentCourseData");
            formData.append("b", cCTE);
            let xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState === 4 && this.status === 200) {
                    let userObject = JSON.parse(xhttp.responseText);
                    // console.log("Institutions", resp)
                    if (userObject.status === "success") {
                        window.courseToEnrollData = userObject;
                    } else {
                        // alert("Failed to add institution because: " + userObject.message);
                    }
                }
            };
            xhttp.open("POST", window.digitalpurse.apiURL);
            xhttp.send(formData);
        },

        tryVC: function (params) {
            // alert("VC CALLED");
            console.warn("VC CALLED Params", params)
        },

        isWalletActive: function () {
            const walletSettings = window.digitalPurseSysDef;
            if (walletSettings.wallet_active === 1 || walletSettings.wallet_active === "1") {
                return true;
            } else {
                return false;
            }
        }
    }
    DigitalPurse.prototype.addCreateWalletButton();
    return DigitalPurse;
});